import {
  createPercent,
  positiveNegativeFormat,
  displayPercent,
  dollarFormat,
  transformAlpacaWebsocketOrder
} from '../../utils';

describe('alpaca', () => {
  describe('createPercent', () => {
    it('Rounds two places when given a decimal', () => expect(createPercent(0.5)).toEqual(50));
    it('Returns NaN if a string is passed in', () => expect(createPercent('test')).toEqual(NaN));
    it('Returns NaN if null', () => expect(createPercent(null)).toEqual(NaN));
  });
  describe('positiveNegativeFormat', () => {
    it('Has bold markdown applied if positive', () => expect(positiveNegativeFormat(1)).toEqual('**1**'));
    it('Has italic markdown applied if negative', () => expect(positiveNegativeFormat(-1)).toEqual('*-1*'));
    it('Returns false if something other than a number is passed', () => expect(positiveNegativeFormat('test')).toEqual(false));
  });
  describe('displayPercent', () => {
    it('Returns bolded markdown two positions right', () => expect(displayPercent(0.123)).toEqual('**12.30**'));
  });
  describe('dollarFormat', () => {
    it('Adds comma and .00', () => expect(dollarFormat(123456)).toEqual('123,456.00'));
    it('Adds two commas for million', () => expect(dollarFormat(1234567)).toEqual('1,234,567.00'));
    it('Adds pads decimals', () => expect(dollarFormat(0.1)).toEqual('0.10'));
    it('Adds trims decimals longer than two places', () => expect(dollarFormat(0.123)).toEqual('0.12'));
  });
  describe('transformAlpacaWebsocketOrder', () => {
    it('converts keys of new order', () => {
      const order = {
        asset_class: 'us_equity',
        asset_id: 'b0b6dd9d-8b9b-48a9-ba46-b9d54906e415',
        canceled_at: null,
        client_order_id: 'cf9afa6a-de5c-4aa2-8b25-0d5f7229120d',
        created_at: '2020-07-27T18:05:06.213431Z',
        expired_at: null,
        extended_hours: false,
        failed_at: null,
        filled_at: null,
        filled_avg_price: null,
        filled_qty: '0',
        hwm: null,
        id: '899af1be-d4f6-4a38-82b2-833987ef82bd',
        legs: null,
        limit_price: null,
        order_class: '',
        order_type: 'market',
        qty: '1',
        replaced_at: null,
        replaced_by: null,
        replaces: null,
        side: 'sell',
        status: 'new',
        stop_price: null,
        submitted_at: '2020-07-27T18:05:06.200253Z',
        symbol: 'AAPL',
        time_in_force: 'day',
        trail_percent: null,
        trail_price: null,
        type: 'market',
        updated_at: '2020-07-27T18:05:06.371078Z'
      };
      const formattedOrder = {
        assetClass: 'us_equity',
        assetId: 'b0b6dd9d-8b9b-48a9-ba46-b9d54906e415',
        canceledAt: null,
        clientOrderId: 'cf9afa6a-de5c-4aa2-8b25-0d5f7229120d',
        createdAt: '2020-07-27T18:05:06.213431Z',
        expiredAt: null,
        extendedHours: false,
        failedAt: null,
        filledAt: null,
        filledAvgPrice: null,
        filledQty: '0',
        hwm: null,
        id: '899af1be-d4f6-4a38-82b2-833987ef82bd',
        legs: null,
        limitPrice: null,
        orderClass: '',
        orderType: 'market',
        qty: '1',
        replacedAt: null,
        replacedBy: null,
        replaces: null,
        side: 'sell',
        status: 'new',
        stopPrice: null,
        submittedAt: '2020-07-27T18:05:06.200253Z',
        symbol: 'AAPL',
        timeInForce: 'day',
        trailPercent: null,
        trailPrice: null,
        type: 'market',
        updatedAt: '2020-07-27T18:05:06.371078Z'
      };
      expect(transformAlpacaWebsocketOrder({ order })).toEqual(formattedOrder);
    });
    it('converts keys of a filled order', () => {
      const order = {
        asset_class: 'us_equity',
        asset_id: 'b0b6dd9d-8b9b-48a9-ba46-b9d54906e415',
        canceled_at: null,
        client_order_id: 'cf9afa6a-de5c-4aa2-8b25-0d5f7229120d',
        created_at: '2020-07-27T18:05:06.213431Z',
        expired_at: null,
        extended_hours: false,
        failed_at: null,
        filled_at: '2020-07-27T18:05:06.249309Z',
        filled_avg_price: '378.47',
        filled_qty: '1',
        hwm: null,
        id: '899af1be-d4f6-4a38-82b2-833987ef82bd',
        legs: null,
        limit_price: null,
        order_class: '',
        order_type: 'market',
        qty: '1',
        replaced_at: null,
        replaced_by: null,
        replaces: null,
        side: 'sell',
        status: 'filled',
        stop_price: null,
        submitted_at: '2020-07-27T18:05:06.200253Z',
        symbol: 'AAPL',
        time_in_force: 'day',
        trail_percent: null,
        trail_price: null,
        type: 'market',
        updated_at: '2020-07-27T18:05:06.473178Z'
      };
      const formattedOrder = {
        assetClass: 'us_equity',
        assetId: 'b0b6dd9d-8b9b-48a9-ba46-b9d54906e415',
        canceledAt: null,
        clientOrderId: 'cf9afa6a-de5c-4aa2-8b25-0d5f7229120d',
        createdAt: '2020-07-27T18:05:06.213431Z',
        expiredAt: null,
        extendedHours: false,
        failedAt: null,
        filledAt: '2020-07-27T18:05:06.249309Z',
        filledAvgPrice: '378.47',
        filledQty: '1',
        hwm: null,
        id: '899af1be-d4f6-4a38-82b2-833987ef82bd',
        legs: null,
        limitPrice: null,
        orderClass: '',
        orderType: 'market',
        qty: '1',
        replacedAt: null,
        replacedBy: null,
        replaces: null,
        side: 'sell',
        status: 'filled',
        stopPrice: null,
        submittedAt: '2020-07-27T18:05:06.200253Z',
        symbol: 'AAPL',
        timeInForce: 'day',
        trailPercent: null,
        trailPrice: null,
        type: 'market',
        updatedAt: '2020-07-27T18:05:06.473178Z'
      };
      expect(transformAlpacaWebsocketOrder({ order })).toEqual(formattedOrder);
    });
  });
});
