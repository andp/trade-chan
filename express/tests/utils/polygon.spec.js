import { transformPolygonData } from '../../utils';

describe('websocket', () => {
  describe('transformPolygonData', () => {
    it('Formats data from the history scrape', () => {
      const historyData = {
        v: 1572520,
        vw: 22.6382,
        o: 23.58,
        c: 22.93,
        h: 23.6,
        l: 21.5,
        t: 1533096000000,
        n: 1
      };
      expect(transformPolygonData({ data: historyData })).toEqual({
        eventType: undefined,
        ticker: undefined,
        tickVolume: 1572520,
        accumulatedDayVolume: undefined,
        dayOpeningPrice: undefined,
        volumeWeightedAveragePrice: 22.6382,
        tickOpenPrice: 23.58,
        tickClosePrice: 22.93,
        tickHighPrice: 23.6,
        tickLowPrice: 21.5,
        tickAveragePrice: undefined,
        tickStartTime: undefined,
        tickEndTime: undefined,
        timestamp: 1533096000000,
        numberOfItems: 1,
        backtester: {
          Date: 1533096000000,
          Open: 23.58,
          Close: 22.93,
          High: 23.6,
          Low: 21.5,
          Volume: 1572520
        }
      });
    });
    it('Formats data from the websocket', () => {
      const websocketData = {
        ev: 'AM', // Event Type ( A = Second Agg, AM = Minute Agg )
        sym: 'MSFT', // Symbol Ticker
        v: 10204, // Tick Volume
        av: 200304, // Accumulated Volume ( Today )
        op: 114.04, // Todays official opening price
        vw: 114.4040, // VWAP (Volume Weighted Average Price)
        o: 114.11, // Tick Open Price
        c: 114.14, // Tick Close Price
        h: 114.19, // Tick High Price
        l: 114.09, // Tick Low Price
        a: 114.1314, // Tick Average / VWAP Price
        s: 1536036818784, // Tick Start Timestamp ( Unix MS )
        e: 1536036818784, // Tick End Timestamp ( Unix MS )
      };
      expect(transformPolygonData({ data: websocketData })).toEqual({
        eventType: 'AM',
        ticker: 'MSFT',
        tickVolume: 10204,
        accumulatedDayVolume: 200304,
        dayOpeningPrice: 114.04,
        volumeWeightedAveragePrice: 114.404,
        tickOpenPrice: 114.11,
        tickClosePrice: 114.14,
        tickHighPrice: 114.19,
        tickLowPrice: 114.09,
        tickAveragePrice: 114.1314,
        tickStartTime: 1536036818784,
        tickEndTime: 1536036818784,
        timestamp: undefined,
        backtester: {
          Close: 114.14,
          Date: 1536036818784,
          High: 114.19,
          Low: 114.09,
          Open: 114.11,
          Volume: 10204
        }
      });
    });
  });
});
