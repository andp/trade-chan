import {
  snakeToCamel
} from '../../utils';

describe('general', () => {
  describe('snakeToCamal', () => {
    it('Works', () => expect(snakeToCamel({ string: 'limit_price' })).toEqual('limitPrice'));
  });
});
