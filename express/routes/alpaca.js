import {
  buyStock, getCash, getPositions, sellStock
} from '../modules/Alpaca';
import { STOCKS } from '../constants';

const alpacaRoutes = async (fastify) => {
  fastify.get('/', async () => ({ hello: 'world' }));

  fastify.post('/:orderType(buy|sell)?', (req, res) => {
    const {
      query: {
        ticker,
        quantity
      },
      params: {
        orderType
      }
    } = req;
    const buySell = orderType === STOCKS.BUY ? buyStock : sellStock;
    buySell({ ticker, quantity }).then(() => {
      console.info(`Executed ${orderType}`);
      return res.sendStatus(200);
    }).catch((ex) => {
      console.error(ex);
      return res.sendStatus(500);
    });
  });

  fastify.get('/cash', async (_, res) => res.send(await getCash()));
  fastify.get('/positions', async (_, res) => res.send(await getPositions()));
};

export default alpacaRoutes;
