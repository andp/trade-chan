module.exports = {
  parser: 'babel-eslint',
  plugins: ['jest'],
  env: {
    node: true,
    es6: true,
    'jest/globals': true
  },
  extends: ['airbnb-base'],
  parserOptions: {
    ecmaVersion: 8,
    babelOptions: {
      configFile: './.babelrc',
    }
  },
  rules: {
    'max-classes-per-file': 0,
    'comma-dangle': 0,
    'linebreak-style': 0,
    'max-len': 0,
    'no-console': [
      'error',
      {
        allow: ['info', 'warn', 'error']
      }
    ],
    'no-bitwise': [
      'error',
      {
        allow: ['~']
      }
    ],
    'import/prefer-default-export': 0
  }
};
