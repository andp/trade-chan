export const parseCommandLine = ([...commandArray] = process.argv) => commandArray.splice(2, commandArray.length);

export const isQuietArgument = () => !!~parseCommandLine().indexOf('quiet');

/**
 * Find command from command line args
 * and split out amount
 * @param command
 * @returns {*|string}
 */
const getCommandLineEntry = ({ command }) => {
  const commandLineArg = parseCommandLine();
  try {
    return commandLineArg.find((commandLine) => commandLine.match(command)).split('=')[1];
  } catch {
    return null;
  }
};

export const getTickerName = () => getCommandLineEntry({ command: 'tickerName' }) || 'FAKETICKER';

export const getTickerAmount = () => getCommandLineEntry({ command: 'tickerAmount' }) || 1;

export const getDataPoints = () => getCommandLineEntry({ command: 'dataPoints' }) || 5;

export const getMinuteInterval = () => getCommandLineEntry({ command: 'minuteInterval' }) || 1;

export const getPubSubName = () => getCommandLineEntry({ command: 'pubSubName' }) || 'testing';
