const getMMFormatMonth = ({ date }) => {
  const month = date.getMonth() + 1;
  if (month.length === 1) return `0${month}`;
  return month;
};

export const getCalendarFormattedDate = ({ date }) => [
  date.getFullYear(),
  getMMFormatMonth({ date }),
  date.getDate()
].join('-');
