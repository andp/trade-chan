export const stripUndefined = ({ object }) => Object.fromEntries(
  Object.entries(object).filter(([, value]) => !!value)
);

/**
 * Transform data structure
 * @param data
 * @returns {
 * {
 *  tickVolume: {Number},
 *  ticker: {String},
 *  tickOpenPrice: {Number},
 *  tickEndTime: {Number},
 *  eventType: {String},
 *  tickAveragePrice: {Number},
 *  tickClosePrice: {Number},
 *  accumulatedDayVolume: {Number},
 *  tickStartTime: {Number},
 *  tickLowPrice: {Number},
 *  dayOpeningPrice: {Number},
 *  tickHighPrice: {Number},
 *  volumeWeightedAveragePrice: {Number}
 * }}
 */
export const transformPolygonData = ({ data }) => {
  const {
    ev: eventType,
    sym: ticker,
    v: tickVolume,
    av: accumulatedDayVolume,
    op: dayOpeningPrice,
    vw: volumeWeightedAveragePrice,
    o: tickOpenPrice,
    c: tickClosePrice,
    h: tickHighPrice,
    l: tickLowPrice,
    a: tickAveragePrice,
    s: tickStartTime,
    e: tickEndTime,
    t: timestamp,
    n: numberOfItems
  } = data;
  return {
    eventType,
    ticker,
    tickVolume,
    accumulatedDayVolume,
    dayOpeningPrice,
    volumeWeightedAveragePrice,
    tickOpenPrice,
    tickClosePrice,
    tickHighPrice,
    tickLowPrice,
    tickAveragePrice,
    tickStartTime,
    tickEndTime
    // timestamp,
    // numberOfItems,
    // backtester: { // backtester data
    //   Date: timestamp || tickStartTime,
    //   Open: tickOpenPrice,
    //   Close: tickClosePrice,
    //   High: tickHighPrice,
    //   Low: tickLowPrice,
    //   Volume: tickVolume
    // }
  };
};
