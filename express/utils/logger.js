import chalk from 'chalk';
import { isQuietArgument } from './commandLine';

// eslint-disable-next-line no-console
const simpleMessage = ({ consoleType, message }) => console[consoleType](message);
// eslint-disable-next-line no-console
const dataMessage = ({ consoleType, message, data }) => console[consoleType](message, data);

const createTimeStamp = () => {
  const date = new Date();
  return `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
};

const colourTimeStamp = ({ colour = 'cyan' }) => chalk[colour](`[${createTimeStamp()}]`);

export const composeLog = (
  {
    message,
    data,
    consoleType = 'info',
    consoleColour
    // ignoreMute = false
  }
) => {
  const composedMessage = `${colourTimeStamp({ colour: consoleColour })} :: ${message}`;
  if (data) return dataMessage({ consoleType, message: composedMessage, data });
  return simpleMessage({ consoleType, message: composedMessage });
};

/**
 * info message
 * @param {string} message
 * @param data
 * @returns {*}
 */
export const logInfo = (
  {
    message,
    data
  }
) => composeLog({
  message,
  data,
  consoleType: 'info',
  consoleColour: 'magenta'
});

/**
 * log error message
 * @param {string} message
 * @param data
 * @returns {*}
 */
export const logError = (
  {
    message,
    data
  }
) => composeLog({
  message,
  data,
  consoleType: 'error',
  consoleColour: 'red'
});
