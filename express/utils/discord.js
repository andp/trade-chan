import Discord from 'discord.js';

export const embedCreate = (
  {
    colour,
    title,
    content,
    footerText
  }
) => new Discord.RichEmbed({
  color: colour,
  title,
  description: content,
  footer: {
    text: footerText
  }
});
