export const snakeToCamel = ({ string }) => string.split('_').map((item, i) => i !== 0 ? item.charAt(0).toUpperCase() + item.slice(1) : item).join('');
