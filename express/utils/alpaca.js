/**
 * Create percentage
 * @param number
 * @returns {number}
 */
import { snakeToCamel } from './general';

export const createPercent = (number) => Number.parseFloat(number) * 100;

/**
 * Apply bold if positive number
 * else apply italic
 * @param number
 * @returns {string}
 */
export const positiveNegativeFormat = (number) => !Number.isNaN(Number(number)) && (number > 0 ? `**${number}**` : `*${number}*`);

/**
 * Round the number to two decimal places
 * then format it
 * @param number
 * @returns {string}
 */
export const displayPercent = (number) => positiveNegativeFormat(createPercent(number).toFixed(2));

/**
 * Dollar format values
 * @param number
 * @returns {string}
 */
export const dollarFormat = (number) => parseFloat(number)
  .toFixed(2)
  .replace(/(\d)(?=(\d{3})+\.)/g, '$1,');

/**
 *
 * @param order
 *
   asset_class: 'us_equity',
   asset_id: 'b0b6dd9d-8b9b-48a9-ba46-b9d54906e415',
   canceled_at: null,
   client_order_id: 'cf9afa6a-de5c-4aa2-8b25-0d5f7229120d',
   created_at: '2020-07-27T18:05:06.213431Z',
   expired_at: null,
   extended_hours: false,
   failed_at: null,
   filled_at: null,
   filled_avg_price: null,
   filled_qty: '0',
   hwm: null,
   id: '899af1be-d4f6-4a38-82b2-833987ef82bd',
   legs: null,
   limit_price: null,
   order_class: '',
   order_type: 'market',
   qty: '1',
   replaced_at: null,
   replaced_by: null,
   replaces: null,
   side: 'sell',
   status: 'new',
   stop_price: null,
   submitted_at: '2020-07-27T18:05:06.200253Z',
   symbol: 'AAPL',
   time_in_force: 'day',
   trail_percent: null,
   trail_price: null,
   type: 'market',
   updated_at: '2020-07-27T18:05:06.371078Z'
 */
export const transformAlpacaWebsocketOrder = ({ order }) => Object.entries(order)
  .reduce((acc, [currKey, value]) => ({ ...acc, [snakeToCamel({ string: currKey })]: value }), {});
