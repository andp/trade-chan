import Alpaca from '@alpacahq/alpaca-trade-api';
import { STOCKS } from '../constants';
import {
  displayPercent,
  positiveNegativeFormat,
  getCalendarFormattedDate,
  dollarFormat,
  chunk
} from '../utils';

const alpaca = new Alpaca({
  paper: true
});

/**
 * Interface with alpaca createOrder function
 *
 * @param {string} ticker
 * @param {string} quantity
 * @param {string} buySell
 * @param {string} orderType
 * @param {string} length
 */
const createOrder = async ({
  ticker,
  quantity,
  buySell = STOCKS.BUY,
  orderType = STOCKS.MARKET,
  length = STOCKS.DAY_LIMIT
}) => alpaca.createOrder({
  symbol: ticker.toUpperCase(), // any valid ticker symbol
  qty: Number.parseInt(quantity, 10),
  side: buySell,
  type: orderType,
  time_in_force: length
});

/**
 * Get account status
 *
 * @returns {Promise<{
 * cash: {Number},
 * equity: {Number},
 * buyingPower: {Number},
 * portfolioValue: {Number},
 * longMarketValue: {Number},
 * shortMarketValue: {Number}
 * }>}
 */
export const getAccount = async () => {
  const {
    cash,
    equity,
    buying_power: buyingPower,
    portfolio_value: portfolioValue,
    long_market_value: longMarketValue,
    short_market_value: shortMarketValue,
    pattern_day_trader: patternDayTrader,
    daytrade_count: dayTradeCount
  } = await alpaca.getAccount();
  return {
    cash,
    equity,
    buyingPower,
    portfolioValue,
    longMarketValue,
    shortMarketValue,
    patternDayTrader,
    dayTradeCount
  };
};

/**
 * Buy stock command
 * @param {string} ticker
 * @param {string} quantity
 * @returns
 */
/*
  {
  id: '47a05299-93fa-4614-9bc6-468ca018a092',
  client_order_id: '6cc7272d-c1a6-436c-9b74-dfa9d516f48e',
  created_at: '2020-07-27T15:14:47.453732Z',
  updated_at: '2020-07-27T15:14:47.453732Z',
  submitted_at: '2020-07-27T15:14:47.406844Z',
  filled_at: null,
  expired_at: null,
  canceled_at: null,
  failed_at: null,
  replaced_at: null,
  replaced_by: null,
  replaces: null,
  asset_id: 'b0b6dd9d-8b9b-48a9-ba46-b9d54906e415',
  symbol: 'AAPL',
  asset_class: 'us_equity',
  qty: '1',
  filled_qty: '0',
  filled_avg_price: null,
  order_class: '',
  order_type: 'market',
  type: 'market',
  side: 'buy',
  time_in_force: 'day',
  limit_price: null,
  stop_price: null,
  status: 'accepted',
  extended_hours: false,
  legs: null,
  trail_percent: null,
  trail_price: null,
  hwm: null
}
 */
export const buyStock = async ({ ticker, quantity }) => createOrder({ ticker, quantity });

/**
 * Sell stock command
 * @param {string} ticker
 * @param {string} quantity
 * @returns
 */
export const sellStock = async ({ ticker, quantity }) => createOrder(
  { ticker, quantity, buySell: STOCKS.SELL }
);

/**
 * Get amount of cash in account
 * @returns {Promise<{Number}>}
 */
export const getCash = async () => {
  const { cash } = await getAccount();
  return cash;
};

/**
 * Check whether account is in margin
 * @returns {Promise<boolean>}
 */
export const isMargin = async () => await getCash() >= 0;

/**
 * Check if trade will put account into margin
 * @param {Number} tradeAmount
 * @returns {Promise<boolean>}
 */
export const willMargin = async ({ tradeAmount }) => await getCash() >= tradeAmount;

/**
 * Check if account is in PDT
 * @returns {Promise<boolean>}
 */
export const isPatternDayTrader = async () => {
  const { patternDayTrader } = await getAccount();
  return patternDayTrader;
};

/**
 * Get text status of account
 * @returns {Promise<string>}
 */
export const getAccountStatus = async () => {
  const {
    cash,
    equity,
    buyingPower,
    portfolioValue,
    longMarketValue,
    shortMarketValue
  } = await getAccount();
  return `
    Cash: ${cash}
    Equity: ${equity}
    Buying Power: ${buyingPower}
    Portfolio Value: ${portfolioValue}
    Long Market Value: ${longMarketValue}
    Short Market Value: ${shortMarketValue}
  `;
};

/**
 * Get text list of positions
 * @returns {Promise<string[]>}
 */
export const getPositions = async () => {
  const holdings = await alpaca.getPositions();
  return holdings.sort(
    (first, second) => first.unrealized_plpc - second.unrealized_plpc
  );
};

/**
 * Format positions for text display in discord
 * @returns {Promise<*>}
 */
export const displayPositions = async () => chunk(await getPositions(), 10).map((positionChunk) => positionChunk.map((position) => {
  const {
    qty: quantity,
    symbol,
    avg_entry_price: avgEntryPrice,
    unrealized_plpc: unrealisedPlPercent,
    unrealized_pl: unrealisedPl,
    unrealized_intraday_plpc: unrealisedDailyPlPercent,
    unrealized_intraday_pl: unrealisedDailyPl
  } = position;
  return [
    `\n${quantity} **${symbol}** bought at ${avgEntryPrice}`,
    `Total P/L %: ${displayPercent(unrealisedPlPercent)} :: Day P/L %: ${displayPercent(unrealisedDailyPlPercent)}`,
    `Total P/L $: ${positiveNegativeFormat(dollarFormat(unrealisedPl))} :: Day P/L $: ${positiveNegativeFormat(dollarFormat(unrealisedDailyPl))}`
  ].join('\n');
}));

/**
 * Get start end end times for specified date range
 * @param {Date} startDate
 * @param {Date} endDate
 * @returns {Promise<[]>}
 */
export const getCalendar = async ({ startDate = new Date(), endDate = new Date() }) => alpaca
  .getCalendar({
    start: getCalendarFormattedDate({ date: startDate }),
    end: getCalendarFormattedDate({ date: endDate })
  });

/**
 * Get next open and close times
 * from Alpaca, and whether or not
 * market is currently open
 *
 * @returns {Promise<{isOpen: *, nextOpen: Date, nextClose: Date}>}
 */
export const getClock = async () => {
  const {
    next_open: nextOpen,
    next_close: nextClose,
    is_open: isOpen
  } = await alpaca.getClock();
  return {
    nextOpen: new Date(nextOpen),
    nextClose: new Date(nextClose),
    isOpen
  };
};
