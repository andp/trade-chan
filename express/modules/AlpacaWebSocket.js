import WebSocket from 'ws';
import { formatInput, transformAlpacaWebsocketOrder } from '../utils';
import { WEBSOCKET } from '../constants';
import Database from './Database';

class AlpacaWebSocket {
  #callbacks = [];

  #isSocketConnected = false;

  /**
   *
   * @param {string} websocketUrl
   * @param {function} messageCallback
   */
  constructor({ websocketUrl, messageCallback = [] }) {
    this.ws = new WebSocket('wss://paper-api.alpaca.markets/stream');
    this.bindWebSocket().then((msg) => {
      console.info(msg);
      this.listen();
    });
  }

  get isAcceptingConnections() {
    return this.#isSocketConnected;
  }

  set #isAcceptingConnections(value) {
    this.#isSocketConnected = value;
  }

  async bindWebSocket() {
    await this.webSocketOpen();
    this.webSocketError();
    this.webSocketMessage();
  }

  addCallback({ callback }) {
    return this.#callbacks.push(callback);
  }

  webSocketOpen() {
    return new Promise((resolve) => {
      this.ws.on('open', () => {
        console.info('Websocket connected');
        this.#isAcceptingConnections = true;
        this.#authenticate();
        resolve();
      });
    });
  }

  /**
   *
   * @return {Function}
   * Data from websocket:
   */
  /*
  {
  stream: 'trade_updates',
  data: {
    event: 'new',
    order: {
      asset_class: 'us_equity',
      asset_id: 'b0b6dd9d-8b9b-48a9-ba46-b9d54906e415',
      canceled_at: null,
      client_order_id: 'cf9afa6a-de5c-4aa2-8b25-0d5f7229120d',
      created_at: '2020-07-27T18:05:06.213431Z',
      expired_at: null,
      extended_hours: false,
      failed_at: null,
      filled_at: null,
      filled_avg_price: null,
      filled_qty: '0',
      hwm: null,
      id: '899af1be-d4f6-4a38-82b2-833987ef82bd',
      legs: null,
      limit_price: null,
      order_class: '',
      order_type: 'market',
      qty: '1',
      replaced_at: null,
      replaced_by: null,
      replaces: null,
      side: 'sell',
      status: 'new',
      stop_price: null,
      submitted_at: '2020-07-27T18:05:06.200253Z',
      symbol: 'AAPL',
      time_in_force: 'day',
      trail_percent: null,
      trail_price: null,
      type: 'market',
      updated_at: '2020-07-27T18:05:06.371078Z'
    }
  }
}

   */
  webSocketMessage() {
    this.ws.on('message', async (data) => {
      const parsedData = JSON.parse(data);
      const {
        data: {
          event,
          order
        }
      } = parsedData;
      if (event === WEBSOCKET.EVENT.NEW) await Database.setRecordInTable({ tableName: 'tradePlaced', data: transformAlpacaWebsocketOrder({ order }) });
      if (event === WEBSOCKET.EVENT.FILL) await Database.setRecordInTable({ tableName: 'tradeFilled', data: transformAlpacaWebsocketOrder({ order }) });
    });
  }

  async #executeCallbacks({data}) {
    if (this.#callbacks.length > 1) {
      return Promise.all(
        this.#callbacks.map(async (callback) => callback(data))
      );
    }
    return this.#callbacks[0](data);
  }

  webSocketError() {
    this.ws.on('error', console.log);
  }

  #authenticate() {
    const command = {
      action: 'authenticate',
      data: {
        key_id: process.env.APCA_API_KEY_ID,
        secret_key: process.env.APCA_API_SECRET_KEY
      }
    };
    this.ws.send(formatInput(command));
  }

  listen() {
    const command = {
      action: 'listen',
      data: {
        streams: ['trade_updates']
      }
    };
    this.ws.send(formatInput(command));
  }
}

export default AlpacaWebSocket;
