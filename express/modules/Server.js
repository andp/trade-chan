import Fastify from 'fastify';
import alpacaRoutes from '../routes/alpaca';

const fastify = Fastify({
  logger: true
});

fastify.register(alpacaRoutes);

const startServer = async (port) => {
  try {
    await fastify.listen(port || process.env.API_PORT);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};

export default startServer;
