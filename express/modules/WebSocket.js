import WebSocket from 'ws';
import {
  formatInput,
  logError,
  logInfo,
  transformPolygonData
} from '../utils';
import { WEBSOCKET } from '../constants';
import Database from './Database';
import { subscribeDatabaseConnection, subscribeTickerAddition, subscribeTickerRemoval } from './Events';

class WebSocketInterface {
  #tickerList = [];

  #opTicker;

  #callbacks = [];

  #isSocketConnected = false;

  #tickersSubscribed = false;

  /**
   *
   * @param {string} websocketUrl
   * @param {function} messageCallback
   */
  constructor({
    websocketUrl = 'wss://alpaca.socket.polygon.io/stocks',
    messageCallback
  }) {
    this.ws = new WebSocket(websocketUrl);
    subscribeDatabaseConnection({
      callback: () => {
        this.#tickers = Database.listOfTickers;
        if (this.#isSocketConnected && !this.#tickersSubscribed) this.subscribeTickers();
      }
    });
    this.#callbacks.push(messageCallback);
    this.prepareWebsocketConnection().then(() => logInfo({ message: 'initial websocket subscription complete' }));
    this.subscribeEventListeners();
  }

  get isAcceptingConnections() {
    return this.#isSocketConnected;
  }

  set #isAcceptingConnections(value) {
    this.#isSocketConnected = value;
  }

  get listOfTickers() {
    return this.#tickers;
  }

  get #tickers() {
    return this.#tickerList;
  }

  set #tickers(value) {
    this.#tickerList = value;
  }

  get #ticker() {
    return this.#opTicker;
  }

  set #ticker(value) {
    this.#opTicker = value.toUpperCase();
  }

  async prepareWebsocketConnection() {
    this.webSocketClose();
    this.webSocketError();
    this.webSocketMessage();
    await this.webSocketOpen().then(() => this.subscribeTickers());
  }

  subscribeEventListeners() {
    subscribeTickerAddition({
      callback: (data) => this.addTicker({ ticker: data.ticker })
    });
    subscribeTickerRemoval({
      callback: (data) => this.removeTicker({ ticker: data.ticker })
    });
  }

  subscribeTickers() {
    if (this.#tickers) this.#tickers.map((ticker) => this.subscribe({ ticker }));
    this.#tickersSubscribed = true;
  }

  addCallback({ callback }) {
    return this.#callbacks.push(callback);
  }

  addTicker({ ticker }) {
    this.#ticker = ticker;
    if (~this.#tickers.indexOf(this.#ticker)) {
      return logInfo({ message: 'This ticker is already being tracked' });
    }
    this.subscribe({ ticker: this.#ticker });
    this.#tickers.push(this.#ticker);
    return logInfo({ message: `Started tracking ${this.#ticker}` });
  }

  removeTicker({ ticker }) {
    this.#ticker = ticker;
    if (!~this.#tickers.indexOf(this.#ticker)) {
      return 'Can\'t remove a ticker that we aren\'t tracking';
    }
    this.unsubscribe({ ticker: this.#ticker });
    this.#tickers.splice(this.#tickers.indexOf(this.#ticker), 1);
    return `Stopped tracking ${this.#ticker}`;
  }

  webSocketOpen() {
    return new Promise((resolve) => {
      this.ws.on('open', () => {
        logInfo({ message: 'Websocket connected' });
        this.#isAcceptingConnections = true;
        this.#authenticate();
        resolve();
      });
    });
  }

  /**
   *
   * @param messageCallback
   * @return {Function}
   * Data from websocket: https://gitlab.com/andp/trade-chan/-/wikis/WebSocket%20Data%20Emission
   */
  webSocketMessage() {
    this.ws.on('message', (data) => {
      const parsedData = JSON.parse(data);
      parsedData.map((msg) => {
        if (msg.ev === 'status') {
          logInfo({
            message: 'Status Update: ',
            data: msg.message
          });
          return this.#executeCallbacks({ data: msg.message });
        }
        const { sym, ev, a } = msg;
        logInfo({
          message: 'Tick: ',
          data: {
            sym,
            ev,
            a
          }
        });
        return this.#executeCallbacks({ data: sym ? transformPolygonData({ data: msg }) : msg });
      });
    });
  }

  async #executeCallbacks({data}) {
    if (this.#callbacks.length > 1) {
      return Promise.all(
        this.#callbacks.map(async (callback) => callback(data))
      );
    }
    return this.#callbacks[0](data);
  }

  webSocketError() {
    this.ws.on('error', (msg) => logError({ message: 'Websocket error', data: msg }));
  }

  webSocketClose() {
    this.ws.on('close', (msg) => {
      logError({ message: 'Websocket closed', data: msg });
      return this.prepareWebsocketConnection().then(() => logInfo({ message: 'reconnected to websocket' }));
    });
  }

  #authenticate() {
    const command = {
      action: 'auth',
      params: process.env.POLY_API_KEY
    };
    this.ws.send(formatInput(command));
  }

  subscribe({ ticker }) {
    const command = {
      action: WEBSOCKET.SUBSCRIBE,
      params: `${WEBSOCKET.AGGREGATE_MINUTE}.${ticker}`
    };
    this.ws.send(formatInput(command));
  }

  unsubscribe({ ticker }) {
    const command = {
      action: WEBSOCKET.UNSUBSCRIBE,
      params: `${WEBSOCKET.AGGREGATE_MINUTE}.${ticker}`
    };
    this.ws.send(formatInput(command));
  }
}

export default WebSocketInterface;
