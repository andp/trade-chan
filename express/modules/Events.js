import EventEmitter from 'events';

class Events extends EventEmitter {}

const events = new Events();

const EVENTS = {
  DATABASE_CONNECTED: 'databaseConnected',
  TICKER_ADDED: 'tickerAdded',
  TICKER_REMOVED: 'tickerRemoved'
};

const subscription = {};

Object.keys(EVENTS).map((event) => events.on(EVENTS[event], (data) => Promise.all(subscription[EVENTS[event]].map((callback) => callback(data)))));

const subscribeEvent = ({ eventName, callback }) => {
  if (!subscription[eventName]) subscription[eventName] = [];
  subscription[eventName].push(callback);
};

export const emitDatabaseConnection = () => events.emit(EVENTS.DATABASE_CONNECTED);
export const subscribeDatabaseConnection = ({ callback }) => {
  subscribeEvent({ eventName: EVENTS.DATABASE_CONNECTED, callback });
};

export const emitTickerAdded = (data) => events.emit(EVENTS.TICKER_ADDED, data);
export const subscribeTickerAddition = ({ callback }) => {
  subscribeEvent({ eventName: EVENTS.TICKER_ADDED, callback });
};

export const emitTickerRemoved = (data) => events.emit(EVENTS.TICKER_REMOVED, data);
export const subscribeTickerRemoval = ({ callback }) => {
  subscribeEvent({ eventName: EVENTS.TICKER_REMOVED, callback });
};
