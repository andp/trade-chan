import request from 'request-promise-native';
import { logError } from '../utils';

/**
 * Get historic data for a ticker
 * at a date from Polygon
 * @param {string} ticker
 * @param {string} date
 * @param {number} limit
 * @returns {Promise<*>}
 */
export const getHistory = async ({ ticker, date, limit }) => {
  const {
    results,
    success,
    db_latency: dbLatency,
    results_count: resultsCount
  } = await request({
    uri: `https://api.polygon.io/v2/ticks/stocks/nbbo/${ticker}/${date}`,
    qs: {
      limit,
      apiKey: process.env.POLY_API_KEY
    },
    json: true
  });
  if (!success) {
    throw Error('Something went wrong');
  }
  console.info(`Found ${resultsCount} results`);
  console.info(`DB Latency: ${dbLatency}`);
  return results;
};

/**
 * Get date span, only works in 1-2 week intervals
 * @param {string} ticker
 * @param {string<YYYY-MM-DD>} startDate
 * @param {string<YYYY-MM-DD>} endDate
 * @param {String<minute, hour, day, week, month, quarter, year>} timespan
 * @param {number} multiplier
 * @returns {Promise<*>}
 */
export const getDateSpanHistory = async (
  {
    ticker,
    startDate,
    endDate,
    timespan = 'minute',
    multiplier = 1
  }
) => {
  const {
    status,
    resultsCount,
    results
  } = await request({
    uri: `https://api.polygon.io/v2/aggs/ticker/${ticker}/range/${multiplier}/${timespan}/${startDate}/${endDate}`,
    qs: {
      apiKey: process.env.POLY_API_KEY
    },
    json: true
  });
  if (status !== 'OK') {
    throw Error('Failure');
  }
  console.info(`Found ${resultsCount} results`);
  return results;
};

/**
 *
 * @param {string} ticker
 * @returns {Promise<{price: any, stockTicker: any, todaysChangePerc: any, updated: any, todaysChange: any}>}
 */
export const getStockPrice = async ({ ticker }) => {
  const {
    ticker: {
      ticker: stockTicker,
      todaysChange,
      todaysChangePerc,
      lastTrade: {
        p: price
      } = {},
      updated
    } = {},
    error
  } = await request({
    uri: `https://api.polygon.io/v2/snapshot/locale/us/markets/stocks/tickers/${ticker.toUpperCase()}`,
    qs: {
      apiKey: process.env.POLY_API_KEY
    },
    json: true
  }).catch((ex) => {
    const {
      statusCode,
      error: {
        status: errorStatus
      }
    } = ex;
    logError({ message: `${statusCode} :: ${errorStatus}` });
    return { error: `${statusCode} :: ${errorStatus}` };
  });
  return {
    stockTicker,
    todaysChange,
    todaysChangePerc,
    price,
    updated,
    error
  };
};
