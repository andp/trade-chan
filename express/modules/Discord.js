import Discord from 'discord.js';
import request from 'request-promise-native';
import Database from './Database';

import {
  buyStock, getAccountStatus, displayPositions, sellStock
} from './Alpaca';
import { getStockPrice } from './Polygon';
import { positiveNegativeFormat, embedCreate } from '../utils';
import { MESSAGE, COMMANDS } from '../constants';
import { emitTickerAdded, emitTickerRemoved } from './Events';

const synonyms = (synonym) => {
  switch (synonym) {
    case 'subscribe':
    case 'sub':
      return COMMANDS.SUBSCRIBE;
    case 'unsubscribe':
    case 'unsub':
      return COMMANDS.UNSUBSCRIBE;
    case 'account':
      return COMMANDS.ACCOUNT;
    case 'price':
    case 'stock':
      return COMMANDS.STOCK;
    case 'buystock':
      return COMMANDS.BUYSTOCK;
    case 'sellstock':
      return COMMANDS.SELLSTOCK;
    case 'positions':
      return COMMANDS.POSITIONS;
    case 'health':
      return COMMANDS.HEALTH;
    default:
      return null;
  }
};

// TODO :: put this elsewhere
const checkPython = async ({ message }) => {
  const result = await request({
    uri: 'http://localhost:5000/health',
    json: true
  });
  const { success } = result;
  if (result) return message.reply('python ping good');
  return message.reply('python ping bad');
};

class DiscordBot {
  constructor() {
    this.bot = new Discord.Client();
  }

  async login() {
    this.bindReady();
    this.bindMessage();
    return this.bot.login(process.env.DISCORD);
  }

  bindReady() {
    this.bot.on('ready', async () => {
      this.sendMessage({ channel: process.env.DISCORD_MONITOR_CHANNEL.toString(), message: 'trade~chan logged in' });
      await this.bot.user.setActivity("w-what's a \"stock\"..?");
    });
  }

  bindMessage() {
    this.bot.on('message', async (message) => {
      // TODO :: abstract this logic out somewhere
      if (!message) return;
      const {
        author: {
          bot: isAuthorBot,
        },
        content: messageContent,
      } = message;
      if (isAuthorBot) return;
      if (!messageContent.startsWith(MESSAGE.PREFIX)) return;

      // TODO :: fix the linting so this stops erroring
      const [command, ticker, quantity] = messageContent
        |> (content) => content.replace(MESSAGE.PREFIX, '')
        |> (content) => content.split(' ');

      switch (synonyms(command)) {
        case COMMANDS.SUBSCRIBE: {
          await Database.setRecordInTable({
            tableName: 'tickers',
            data: { ticker }
          });
          emitTickerAdded({ ticker });
          await message.reply(`maybe added: ${ticker}`);
          break;
        }
        case COMMANDS.UNSUBSCRIBE: {
          await Database.deleteRecordInTable(
            {
              tableName: 'tickers',
              data: { ticker }
            }
          );
          emitTickerRemoved({ ticker });
          await message.reply(`maybe removes: ${ticker}`);
          break;
        }
        case COMMANDS.ACCOUNT: {
          await message.reply(await getAccountStatus());
          break;
        }
        case COMMANDS.STOCK: {
          const {
            stockTicker,
            todaysChange,
            todaysChangePerc,
            price,
            error
          } = await getStockPrice({ ticker });
          if (error) {
            await message.reply(error);
            break;
          }
          const embed = embedCreate({
            colour: 0xff77d9,
            title: stockTicker,
            content: `
            $${price}
            
            Day Change:
            $${positiveNegativeFormat(todaysChange)}
            %${positiveNegativeFormat(todaysChangePerc)}`
          });
          await message.reply(embed);
          break;
        }
        case COMMANDS.POSITIONS: {
          const positions = await displayPositions();
          positions.map((positionChunk) => message.reply(positionChunk));
          break;
        }
        // TODO :: dedupe
        case COMMANDS.BUYSTOCK: {
          const {
            side,
            order_type: orderType,
            qty,
            symbol
          } = await buyStock({ ticker, quantity });
          await message.reply(`placed ${side} ${orderType} order for ${qty} shares of ${symbol}`);
          break;
        }
        case COMMANDS.SELLSTOCK: {
          const {
            side,
            order_type: orderType,
            qty,
            symbol
          } = await sellStock({ ticker, quantity });
          await message.reply(`placed ${side} ${orderType} order for ${qty} shares of ${symbol}`);
          break;
        }
        case COMMANDS.HEALTH: {
          await checkPython({ message }).catch(() => message.reply('python ping no good'));
          break;
        }
        default:
          break;
      }
    });
  }

  sendMessage({ channel = process.env.DISCORD_TRADE_CHANNEL.toString(), message }) {
    return this.bot.channels.get(channel).send(message);
  }
}

// TODO :: look at .mjs to see if the returned result of `DiscordBot.login()` can be exported
export default new DiscordBot();
