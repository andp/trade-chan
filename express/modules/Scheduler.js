import cron from 'cron';
import { getClock } from './Alpaca';
import { postBrain } from './Requests';
import { BRAIN } from '../constants';

export const initMarketJobs = async () => {
  const { nextOpen, nextClose } = await getClock();
  const jobs = [
    cron.job(nextOpen, () => postBrain({ body: { marketOpen: true, endpoint: BRAIN.MARKET } })),
    cron.job(nextClose, () => postBrain({ body: { marketOpen: false, endpoint: BRAIN.MARKET } }))
  ];
  jobs.forEach((job) => job.start());
};

export const initScheduler = async () => {
  await initMarketJobs().catch(() => console.error('Problem starting initial market time watch'));
  console.info('Started initial market time watch');
  const checkMarket = cron.job('0 4 * * *', () => initMarketJobs());
  checkMarket.start();
};
