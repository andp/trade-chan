import request from 'request-promise-native';

export const postBrain = ({ body, endpoint }) => request({
  method: 'POST',
  uri: `http://${process.env.BRAIN_IP}${process.env.BRAIN_PORT && `:${process.env.BRAIN_PORT}`}/${endpoint}`,
  body,
  json: true
}).catch((ex) => console.error(ex));

export const getBrain = ({ endpoint }) => request({
  method: 'GET',
  uri: `${process.env.BRAIN_IP}${process.env.BRAIN_PORT && `:${process.env.BRAIN_PORT}`}/${endpoint}`,
  json: true
});
