import createSubscriber from 'pg-listen';
import schemaInspector from 'knex-schema-inspector';
import knex from 'knex';
import { logError, logInfo } from '../utils';
import { emitDatabaseConnection } from './Events';

class Database {
  #knex;

  #inspector;

  #tables;

  #tickers;

  #subscriber;

  constructor() {
    this.init().then(() => console.info('Database connected'));
  }

  async init() {
    const postgresUrl = `postgres://${process.env.DB_USER}${process.env.DB_PASS ? `:${process.env.DB_PASS}` : ''}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`;

    this.#knex = await knex({
      client: 'pg',
      connection: {
        host: process.env.DB_HOST,
        port: process.env.DB_PORT,
        database: process.env.DB_NAME,
        user: process.env.DB_USER,
        ...(process.env.DB_PASS && { password: process.env.DB_PASS })
      },
      searchPath: ['public']
    });

    this.#subscriber = createSubscriber({ connectionString: postgresUrl });
    this.#inspector = schemaInspector(this.#knex);

    await Promise.all([
      this.setTableList(),
      this.setTickerList(),
      this.#subscriber.connect()
    ]);
    logInfo({ message: 'Database tables', data: this.#tables });
    emitDatabaseConnection();
  }

  get listOfTickers() {
    return this.#tickers;
  }

  async setTableList() {
    this.#tables = await this.#inspector.tables();
    return null;
  }

  async setTickerList() {
    try {
      const tickers = await this.#knex.from('tickers');
      this.#tickers = tickers.map(({ ticker }) => ticker);
    } catch (e) {
      logError({ message: e });
    }
  }

  /**
   * Check if ticker exists
   * @param {String} tableName
   * @returns {boolean}
   */
  async doesTableExist({ tableName }) {
    return this.#inspector.hasTable(tableName);
  }

  async #createTickerTable({ tableName, data }) {
    return this.#knex.schema.createTableIfNotExists(tableName, (table) => {
      table.increments('id').primary();
      table.string('ticker');
      table.string('eventType');
      table.bigint('tickVolume');
      table.bigint('tickStartTime');
      table.bigint('tickEndTime');
      table.bigint('accumulatedDayVolume');
      table.specificType('dayOpeningPrice', 'double precision').notNull()
      table.specificType('volumeWeightedAveragePrice', 'double precision').notNull()
      table.specificType('tickOpenPrice', 'double precision').notNull()
      table.specificType('tickClosePrice', 'double precision').notNull()
      table.specificType('tickHighPrice', 'double precision').notNull()
      table.specificType('tickLowPrice', 'double precision').notNull()
      table.specificType('tickAveragePrice', 'double precision').notNull()
    }).then(() => {
      logInfo({ message: `Created table for ${tableName}` });
      return this.#knex(tableName).insert(data);
    });
  }

  async createTable({ tableName, callback }) {
    if (!await this.doesTableExist({ tableName })) return this.#knex.schema.createTableIfNotExists(tableName, (table) => callback(table));
    return null;
  }

  async alertChannel({ channelName, data }) {
    return this.#subscriber.notify(channelName, { ...data });
  }

  /**
   * Set data for tickers into table
   * @param tableName - ticker name
   * @param data
   * @param shouldNotify
   * @param notifyChannelName
   * @returns {Promise<*>}
   */
  async setStockData({
    tableName,
    data,
    shouldNotify = true,
    notifyChannelName = 'stockadded'
  }) {
    if (!(await this.doesTableExist({ tableName }))) {
      return this.#createTickerTable({ tableName, data });
    }
    return this.setRecordInTable({
      tableName,
      data,
      shouldNotify,
      notifyChannelName,
      notifyData: {
        ticker: tableName
      }
    });
  }

  /**
   * Set data into table
   * @param {string} tableName - name of database table
   * @param {object} data - data
   * @param {boolean} [shouldNotify]
   * @param {string} [notifyChannelName]
   * @param {object} [notifyData]
   * @returns {Promise<*>}
   */
  async setRecordInTable({
    tableName,
    data,
    shouldNotify,
    notifyChannelName,
    notifyData
  }) {
    try {
      await this.#knex(tableName).insert(data);
    } catch (ex) {
      logError({ message: `error inserting data into ${tableName}`, data: ex });
    }
    if (shouldNotify && notifyChannelName) {
      return this.alertChannel(
        {
          channelName: notifyChannelName,
          data: notifyData
        }
      ).catch((ex) => console.error(ex));
    }
    return null;
  }

  async deleteRecordInTable({ tableName, data }) {
    if (!await this.doesTableExist({ tableName })) return knex(tableName).where(data).del();
    return null;
  }
}

export default new Database();
