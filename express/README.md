Run `yarn` to install packages

Create a `.env` file and fill in the values shown in the example

Start up a new postgres database

Run for dev:
* `yarn dev`

Run for prod

* `yarn start`

For Windows.
npm install
npm run dev