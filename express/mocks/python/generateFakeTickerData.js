import { addMinutes } from 'date-fns';

const generateVolume = () => Math.floor(Math.random() * 1000000);

const roundToThreeDecimal = ({ number }) => Number(Number(number).toFixed(3));

const plusMinus = ({ baseNumber, secondNumber }) => {
  if (Math.floor(Math.random() * Math.floor(2))) {
    return Math.abs(roundToThreeDecimal({ number: (baseNumber + secondNumber) }));
  }
  return Math.abs(roundToThreeDecimal({ number: (baseNumber - secondNumber) }));
};

const generateTickPrice = ({ tickerIndex }) => {
  const tickerPrice = roundToThreeDecimal({ number: tickerIndex + Math.random() });
  const tickHighPrice = roundToThreeDecimal({ number: Number(tickerPrice + (tickerIndex / (tickerIndex / 2))) });
  const tickLowPrice = roundToThreeDecimal({
    number: Math.abs(Number(tickerPrice - (tickerIndex / (tickerIndex / 2))))
  });
  const tickClosePrice = plusMinus({
    baseNumber: tickerPrice,
    secondNumber: (tickHighPrice + tickLowPrice) / 2
  });
  return {
    tickOpenPrice: tickerPrice,
    tickClosePrice,
    tickHighPrice,
    tickLowPrice,
    tickAveragePrice: roundToThreeDecimal({ number: (tickerPrice + tickClosePrice + tickHighPrice + tickLowPrice) / 4 })
  };
};

const generateTickerValues = ({
  ticker,
  tickerIndex,
  startTime,
  minuteInterval,
  dataPointsPerTicker
}) => {
  const dataPoints = [];
  for (let i = 0; i <= dataPointsPerTicker; i += 1) {
    const {
      tickOpenPrice,
      tickClosePrice,
      tickHighPrice,
      tickLowPrice,
      tickAveragePrice
    } = generateTickPrice({ tickerIndex });
    const tickStartTime = Date.parse(addMinutes(startTime, minuteInterval * i));
    dataPoints.push({
      eventType: 'AM',
      ticker,
      tickVolume: generateVolume(),
      tickStartTime,
      tickOpenPrice,
      tickClosePrice,
      tickHighPrice,
      tickLowPrice,
      // ignored
      accumulatedDayVolume: generateVolume(),
      dayOpeningPrice: 0,
      volumeWeightedAveragePrice: 0,
      tickAveragePrice,
      tickEndTime: tickStartTime
    });
  }
  return dataPoints;
};

export const generateTickerKey = ({
  tickerName,
  tickerAmount,
  minuteInterval,
  dataPointsPerTicker
}) => {
  const dataObject = {};
  for (let i = 0; i <= tickerAmount; i += 1) {
    const ticker = `${tickerName}${i}`;
    const startTime = new Date();
    Object.assign(dataObject, {
      [ticker]: generateTickerValues({
        ticker,
        tickerIndex: i + 1,
        startTime,
        minuteInterval,
        dataPointsPerTicker
      })
    });
  }
  return dataObject;
};
