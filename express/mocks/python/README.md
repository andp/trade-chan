To generate test data to route through the database to trigger postgres pub/sub run `yarn testPython`

This command accepts 4 arguments, if no arguments are passed then a default value will be used:

* `tickerName=STRING | FAKETICKER` - this should be all caps
* `tickerAmount=INT | 1` - 0 indexed
* `dataPoints=INT | 5`
* `minuteInterval=INT | 1`
* `pubSubName=STRING | testing` - this should be all lowercase

Example command `yarn testPython tickerName=DADDY tickerAmount=69 dataPoints=420 minuteInterval=4 pubSubName=yamero`

All of the test data will be written to `express/tests/python/logs` with a filename of `${tickerName}-${unixTimestamp}.json`
