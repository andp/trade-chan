import fs from 'fs';
import path from 'path';
import Database from '../../modules/Database';
import {
  getDataPoints,
  getMinuteInterval,
  getPubSubName,
  getTickerAmount,
  getTickerName
} from '../../utils/commandLine';
import { generateTickerKey } from './generateFakeTickerData';
import { logInfo } from '../../utils';
import { subscribeDatabaseConnection } from '../../modules/Events';

// import { server } from '../../app';

const fakeTicker = getTickerName();
const tickerAmountArg = getTickerAmount();
const dataPointsPerTickerArg = getDataPoints();
const minuteIntervalArg = getMinuteInterval();
const pubSubNameArg = getPubSubName();

logInfo({
  message: `
Running with arguments:
ticker: ${fakeTicker}
amount of tickers: ${tickerAmountArg}
amount of datapoints per ticker: ${dataPointsPerTickerArg}
minute interval: ${minuteIntervalArg}
postgres pubsub name: ${pubSubNameArg}
  `
});

const dataObject = generateTickerKey({
  tickerName: fakeTicker,
  tickerAmount: tickerAmountArg,
  minuteInterval: minuteIntervalArg,
  dataPointsPerTicker: dataPointsPerTickerArg
});

const dispatchData = ({ minuteInterval, pubSubName }) => {
  Object.values(dataObject).forEach((dataEntry) => {
    dataEntry.map((tickerEntry, index) => {
      const timer = ((60 * 1000) * (minuteInterval + index));
      return setTimeout(() => {
        logInfo({ message: `Logging ${tickerEntry.ticker}`, data: tickerEntry });
        return Database.setStockData({
          tableName: tickerEntry.ticker,
          data: tickerEntry,
          notifyChannelName: pubSubName
        });
      }, timer);
    });
  });
};

const writeDataToFile = ({ tickerName }) => {
  const unixTimeStamp = Date.parse(new Date());
  fs.writeFileSync(path.join(__dirname, `/logs/${tickerName}-${unixTimeStamp}.json`), JSON.stringify(dataObject));
  logInfo({ message: `data written to ${tickerName}-${unixTimeStamp}.json` });
};

// server.listen(5000, (err) => {
//   if (err) throw err;
//   logInfo({ message: '> Ready on http://localhost:', data: 5000 });
// });

const createDatabaseTables = () => Database.createTable({
  tableName: 'tickers',
  callback: (returnedTable) => returnedTable && returnedTable.string('ticker').primary()
});

const init = async () => {
  await createDatabaseTables();

  dispatchData({
    minuteInterval: minuteIntervalArg,
    pubSubName: pubSubNameArg
  });

  writeDataToFile({ tickerName: fakeTicker });
};

subscribeDatabaseConnection({ callback: () => init() });
