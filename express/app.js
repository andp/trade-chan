import express from 'express';
import WebSocketInterface from './modules/WebSocket';
import DiscordBot from './modules/Discord';
import Database from './modules/Database';
import {
  buyStock, getCash, getPositions, sellStock
} from './modules/Alpaca';
import { initScheduler } from './modules/Scheduler';
import { getDateSpanHistory, getHistory } from './modules/Polygon';
import { STOCKS, WEBSOCKET } from './constants';
import { transformPolygonData, logInfo } from './utils';

// import startServer from './modules/Server';
//
// startServer(6000);

/**
 * Initiate scheduler to check market times
 */
initScheduler().then(() => logInfo({ message: 'started scheduler' }));

/**
 * Initialise websocket
 */
const webSocket = new WebSocketInterface({
  messageCallback: async (data) => {
    const { ticker } = data;
    if (ticker) {
      await Database.setStockData({ tableName: ticker, data });
    }
  }
});

/**
 * Initialise DiscordBot
 * @type {DiscordBot}
 */
DiscordBot.login().then(() => logInfo({ message: 'discord bot logged in' }));

/**
 * Postgres 1
 *  buy/sell signal
 * Backtesting Framework 2
 */

/**
 * Setup API
 */
export const server = express();

/**
 * Alpaca endpoints
 */
server.post('/:orderType(buy|sell)?', (req, res) => {
  const {
    query: {
      ticker,
      quantity
    },
    params: {
      orderType
    }
  } = req;
  const buySell = orderType === STOCKS.BUY ? buyStock : sellStock;
  buySell({ ticker, quantity }).then(() => {
    // TODO :: need to dump this to DB
    console.info(`Executed ${orderType}`);
    // send confirmation
    DiscordBot.sendMessage({ message: `executed **${orderType}** for ${quantity} share(s) of ${ticker}` });
    return res.sendStatus(200);
  }).catch((ex) => {
    console.error(ex);
    // send error
    DiscordBot.sendMessage({ message: ex.response.toJSON().body.message });
    return res.sendStatus(500);
  });
});
server.post('/sellSignal', () => {});
server.post('/buySignal', () => {});

server.get('/cash', async (_, res) => res.send(await getCash()));

server.get('/positions', async (_, res) => res.send(await getPositions()));

/**
 * Polygon endpoints
 */
server.post('/getHistoricalData', (req, res) => {
  const {
    query: {
      ticker,
      date,
      limit
    }
  } = req;
  getHistory({ ticker, date, limit }).then(
    (results) => res.send(results)
  ).catch(
    () => res.sendStatus(500)
  );
});
server.post('/getDateSpanHistory', (req, res) => {
  const {
    query: {
      ticker,
      timespan,
      startDate,
      endDate,
      multiplier
    }
  } = req;
  getDateSpanHistory(
    {
      ticker,
      startDate,
      endDate,
      timespan,
      multiplier
    }
  ).then(
    (results) => res.send(results.map((result) => transformPolygonData({ data: result })))
  ).catch(
    () => res.sendStatus(500)
  );
});

/**
 * Websocket endpoints
 */
server.get('/tickerList', (_, res) => res.send(webSocket.listOfTickers));
server.post('/ticker/:tickerAction(add|remove)?', (req, res) => {
  const {
    query: {
      ticker
    },
    params: {
      tickerAction
    }
  } = req;
  if (tickerAction === WEBSOCKET.ADD) webSocket.addTicker({ ticker });
  else webSocket.removeTicker({ ticker });
  // TODO :: need to add some handling so this doesn't 200 if it doesn't fail
  res.sendStatus(200);
});

/**
 * Testing endpoints
 */
server.get('/ping', (_, res) => res.send('pong'));
server.post('/ping', (_, res) => res.send('pong'));

server.get('/serverPing', (_, res) => {
  DiscordBot.sendMessage({ message: 'get ping from python' });
  res.send(200);
});
server.post('/serverPing', (_, res) => {
  DiscordBot.sendMessage({ message: 'post ping from python' });
  res.send(200);
});

const port = parseInt(process.env.API_PORT, 10);
server.listen(port, (err) => {
  if (err) throw err;
  console.info(`> Ready on http://localhost:${port}`);
});
