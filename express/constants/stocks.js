export default {
  BUY: 'buy',
  SELL: 'sell',
  MARKET: 'market',
  DAY_LIMIT: 'day'
};
