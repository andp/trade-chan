export default {
  SUBSCRIBE: 'subscribe',
  UNSUBSCRIBE: 'unsubscribe',
  AGGREGATE_MINUTE: 'AM',
  ADD: 'add',
  REMOVE: 'remove',
  EVENT: {
    NEW: 'new',
    FILL: 'fill'
  }
};
