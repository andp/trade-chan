"""
Script to Compare Express Generated Test Logs from
npm run testPython

To the Database output in PostgreSQL

To the Logged Files from
postgresData.py
"""

import os
import pandas as pd
import json
import psycopg2
import numpy as np
import datetime

def loadExpress(fileLoc):
    #Holding our Data to Return
    returnData = []
    #Turns an Express Log File (JSON) into a Pandas Dataframe
    with open(fileLoc,'r') as inFile:
        incomingData = json.loads(inFile.read())
    #Breaking out the Different Tables
    for ticker in incomingData.keys():
        df = pd.DataFrame(incomingData[ticker])
        #Converting the Timestamp to a Datetime Object
        df['Date'] = pd.to_datetime(df['tickStartTime'], unit='ms', origin='unix')
        #Renaming the Columns to our Common Name
        df['Open'] = df['tickOpenPrice']
        df['Close'] = df['tickClosePrice']
        df['Low'] = df['tickLowPrice']
        df['High'] = df['tickHighPrice']
        df['Volume'] = df['tickVolume']
        df['ticker'] = ticker
        df = df.set_index('Date')
        #Getting only the Columns we Want
        df = df[['Open','High','Low','Close','Volume','ticker']].copy()
        returnData.append(df)
    return pd.concat(returnData, axis=0)

def findExpressLogs():
    #Uses the Current Working Directory to Find the Express Directory.
    expressLogDir = os.path.sep.join(os.getcwd().split(os.path.sep)[:-1] + ['express','tests','python','logs'])
    return [os.path.join(expressLogDir,i) for i in os.listdir(expressLogDir)]

def getDatabaseData(tickers):
    #Opening the Connection to Start
    #Finding the .env file
    with open('.env','r') as inFile:
        lines = inFile.read()
        lines = lines.split('\n')
    loginDetails = {}
    loginDetails['dbname'] = [i.split('=')[-1] for i in lines if 'db_name' in i.lower()][0]
    loginDetails['user'] = [i.split('=')[-1] for i in lines if 'db_user' in i.lower()][0]
    loginDetails['password'] = [i.split('=')[-1] for i in lines if 'db_pass' in i.lower()][0]
    loginDetails['host'] = [i.split('=')[-1] for i in lines if 'db_host' in i.lower()][0]
    loginDetails['port'] = [i.split('=')[-1] for i in lines if 'db_port' in i.lower()][0]
    #Connecting to the Database.
    connection = psycopg2.connect(dbname = loginDetails['dbname'], user = loginDetails['user'], password = loginDetails['password'], host = '192.168.1.100', port = int(loginDetails['port']))
    #Importing our Stock information from progresData
    from postgresData import stock
    stocks = [stock(i,connection) for i in tickers]
    #Closing the Connection
    connection.close()
    return pd.concat([i.df[['Open','High','Low','Close','Volume','ticker']].copy() for i in stocks])

def findPythonLogs():
    #Gets all of the Logs in the Python Folder
    pythonLogDir = os.path.sep.join(os.getcwd().split(os.path.sep) + ['testingLogs'])
    return [os.path.join(pythonLogDir,i) for i in os.listdir(pythonLogDir)]

def loadPython(fileLoc):
    #Loads a Python Result
    return pd.read_csv(fileLoc)

#Compares the Express Logs against the Database.
def compareDatabase():
    #Loading in the Express
    expressData = pd.concat([loadExpress(i) for i in findExpressLogs()])
    expressData = expressData.drop_duplicates()
    expressData.sort_index(inplace=True)
    #Getting the Unique Tickers to get the Database Data.
    databaseData = getDatabaseData(expressData.ticker.unique())
    databaseData.sort_index(inplace=True)
    """
    badData = compareTables(expressData,databaseData)
    print('Checking Data Entry into Database')
    if badData:
        print('\n'.join([str(i) for i in badData]))
    else:
        print('Data Entry into Database is Good')
    #Checking the Python Side of Things
    print('Checking Python Data')
    """
    pythonWrong = False
    for fileName in findPythonLogs():
        pythonData = loadPython(fileName)
        badData = compareTables(pythonData,databaseData)
        if badData:
            print('Found Missmatch Data in - ' + fileName)
            print('\n'.join([str(i) for i in badData]))
            pythonWrong = True
        break
    if not pythonWrong:
        print('Data Entry into Python is good')
    #print(expressData.compare(databaseData))
    
def compareTables(groundTruth,comparison):
    #Holding our Bad Rows.
    badrows = []
    #Concating the Data
    for index, row in comparison.iterrows():
        if not findMatchingRow(row,index,groundTruth):
            badrows.append(row)
            break
        #Grabbing our Data into a Numpy Array
        #comparisonRow = np.append(row.values,unix_time_millis(index))
        #Seeing if we've got any matches to the ground truth.
        #print(comparisonRow)
        #print(any([np.array_equal(comparisonRow,np.append(r.values,unix_time_millis(i))) for i,r in groundTruth.iterrows()]))
            #badrows.append(comparisonRow)
    return badrows

def findMatchingRow(row,index,df):
    #Looking via Index to Start
    filteredDf = df[df.index == index]
    filteredDf = filteredDf[filteredDf['ticker'] == row['ticker']]
    matchingRow = False
    print(filteredDf)
    for matchIndex,matchRow in filteredDf.iterrows():
        print([round(matchRow[k],3) == round(row[k],3) for k in matchRow.keys() if k != 'ticker'])
        matchingRow = all([round(matchRow[k],3) == round(row[k],3) for k in matchRow.keys() if k != 'ticker'])
        if matchingRow:
            break
    return matchingRow

def unix_time_millis(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds() * 1000.0

if __name__ == "__main__":
    #expressFileloc = findExpressLogs()[0]
    #print(loadExpress(expressFileloc))
    #print(getDatabaseData(['FAKETICKER0']))
    #loadPython(findPythonLogs()[0])
    compareDatabase()
    pass