import uuid

"""
Base SimLog, creates a process ID, starting and ending time
Holds abilities about writing process logs
"""
class simLog():
    #Where we're saving our logs out
    logDir = 'simLogs'
    def __init__(self, pid):
        import datetime
        self.pid = pid
        self.start = datetime.datetime.now()
        self.end = None
        #Write out the Initial Log
        self._writeLog()
    #Writes a Simlog to the Directory
    def _writeLog(self):
        import os
        import json
        from bson import json_util
        #Making the Directory for the Logs if it doesn't exist
        if self.logDir not in os.listdir('.'):
            os.mkdir(self.logDir)
        #Dumping the JSON Information
        with open(os.path.join(self.logDir,self.pid)+'.json','w') as outFile:
            outFile.write(json.dumps(self._buildDict(), default=json_util.default))
    #Function to run once the Process has completed.
    def complete(self):
        import datetime
        #Finalizing the Finish Time and Logging.
        self.end = datetime.datetime.now()
        self._writeLog()

"""
Holds the Log for a Backtesting Event
Adds additional data about the items in the Test
"""
class backtestLog(simLog):
    def __init__(self,algorithm,datasets,pid):
        self.algorithm = algorithm
        self.datasets = datasets
        simLog.__init__(self, pid = pid)
    #Builds the Dict Data off the Object for us to Log
    def _buildDict(self):
        objDict = {'task':'backtest',
                    'ID':self.pid,
                    'Datasets':self._dictDatasets(),
                    'Algorithm':self._dictAlgorithm(),
                    'Start':self.start,
                    'End':self.end}
        return objDict
    #Makes the Dict Object for the Datasets
    def _dictDatasets(self):
        return [i.returnData() for i in self.datasets]
    #Makes the Dict Object for the Algorithm
    def _dictAlgorithm(self):
        return {'name':self.algorithm.__name__,
                'description':self.algorithm.__doc__}


"""
Holds the Information about a Completed Simulation
Takes in Process ID from matching backtestLog, and the resulting Pandas Dataframe from the Simulation
Shares the outcome from all the Sims
"""
class resultsLog(simLog):
    def __init__(self,pid,resultDF):
        self.resultDF = resultDF
        pid += '_Results'
        simLog.__init__(self,pid = pid)
    #Function to Save out the Data
    def _buildDict(self):
        #Removing the Results segment from it.
        self.resultDF = self.resultDF.drop('_strategy')
        self.resultDF = self.resultDF.T 
        #We're trimming _Results off PID in this
        objDict = {'task':'backtest',
                    'ID':self.pid[:-8],
                    'results':self.resultDF.to_json(orient='records')}
        return objDict