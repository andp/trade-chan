import pandas as pd

"""
Runs a backtest on a Given Algorithm and Dataset
Outputs to results, and simLog.json to give information on the runs
"""
class test():
    def __init__(self,algorithm,dataset):
        #Saving the Algorithm and our Data
        self.algo = algorithm
        self.data = dataset
    #Initiates the Backtest
    def test(self):
        #Starting a Log File
        import simLogger
        import uuid
        logFile = simLogger.backtestLog(self.algo,self.data,str(uuid.uuid4()))
        #Simple Backtest on 2 datasets.
        
        import concurrent.futures
        #Futures for getting the Results of the Simulated Threads
        futures = []
        #Setting up our Threads to Execute the Backtests
        with concurrent.futures.ThreadPoolExecutor() as executor:
            for dat in self.data:
                #Running the Backtest-ish
                #This should be put into process Daemons shortly.
                futures.append(executor.submit(self._runTest, dat,self.algo))
                #bt = backtesting.Backtest(dat.dataFrame,self.algo)
                #res = bt.run()
                #Format the Results before Appending
                #res = self._formatResults(res,dat)
                #results.append(res)
        results = [i.result() for i in futures]
        logFile.complete()
        df = pd.concat(results, axis=1)
        #Making a log file for the Results
        simLogger.resultsLog(logFile.pid,df)
        del simLogger
        return df
    #Runs a Single Test
    def _runTest(self,dataObj,algo):
        import backtesting
        bt = backtesting.Backtest(dataObj.dataFrame,self.algo)
        res = bt.run()
        #Format the Results before Appending
        res = self._formatResults(res,dataObj)
        return res
    #formatting the Results
    def _formatResults(self,results,dataSet):
        #Adding Information about The Data used for Testing
        results['Group'] = dataSet.group
        results['Dataset'] = dataSet.name
        results['Ticker'] = dataSet.ticker
        #Looking up information about our Algorithm
        import simAlgorithms
        #Getting our Variables to Optimize and listing out what we just tested at
        strategies = simAlgorithms.algorithmInformation()
        optimizationMapping = strategies.getStrategyVariables(self.algo.__name__)
        if optimizationMapping:
            for optimizeVar in optimizationMapping:
                results[optimizeVar] = getattr(self.algo,optimizeVar)
        return results
"""
"""Trying a Non-Class Based Test Run
#formatting the Results
def formatResults(algo,results,dataSet):
    #Adding Information about The Data used for Testing
    results['Group'] = dataSet.group
    results['Dataset'] = dataSet.name
    results['Ticker'] = dataSet.ticker
    #Looking up information about our Algorithm
    import simAlgorithms
    #Getting our Variables to Optimize and listing out what we just tested at
    strategies = simAlgorithms.algorithmInformation()
    optimizationMapping = strategies.getStrategyVariables(algo.__name__)
    if optimizationMapping:
        for optimizeVar in optimizationMapping:
            results[optimizeVar] = getattr(algo,optimizeVar)
    return results

#Runs a Single Test
def runTest(dataObj,algo):
    import backtesting
    bt = backtesting.Backtest(dataObj.dataFrame,algo)
    res = bt.run()
    #Format the Results before Appending
    res = formatResults(algo,res,dataObj)
    return res

def testFunction(algorithm,dataset):
    #Starting a Log File
        import simLogger
        import uuid
        logFile = simLogger.backtestLog(algorithm,dataset,str(uuid.uuid4()))
        #Simple Backtest on 2 datasets.
        
        import concurrent.futures
        #Futures for getting the Results of the Simulated Threads
        futures = []
        #Setting up our Threads to Execute the Backtests
        with concurrent.futures.ThreadPoolExecutor() as executor:
            for dat in dataset:
                #Running the Backtest-ish
                futures.append(executor.submit(runTest, dat,algorithm))
        #Appending all the Results together
        results = [i.result() for i in futures]
        logFile.complete()
        df = pd.concat(results, axis=1)
        #Making a log file for the Results
        simLogger.resultsLog(logFile.pid,df)
        del simLogger
        return df
