"""File Organizes Data Coming in from the Websocket"""
"""
{
    "eventType": "AM",
    "ticker": "MSFT",
    "tickVolume": 10204,
    "accumulatedDayVolume": 200304,
    "dayOpeningPrice": 114.04,
    "volumeWeightedAveragePrice": 114.4040,
    "tickOpenPrice": 114.11,
    "tickClosePrice": 114.14,
    "tickHighPrice": 114.19,
    "tickLowPrice": 114.09,
    "tickAveragePrice": 114.1314,
    "tickStartTime": 1536036818784,
    "tickEndTime": 1536036818784
}
"""
import os,pickle
import pandas as pd
import datetime
import math
import datetime
#Stops it from sending buy/sell to the server if False
SERVER_CONNECTION = True

class dataOrganizer():
    def __init__(self,cacheDir = os.path.join('testingData','liveData.p'),interval = 1):
        #If we've got a Cache, load the Data
        self.cacheDir = cacheDir
        if os.path.exists(cacheDir):
            self._loadData()
        #If not, we'll start empty
        else:
            self.data = {'stocks':[],
                    'assets':[]}
    """Simple Saving and Loading Functions"""
    #Saves out all the Data
    def _saveData(self):
        with open(self.cacheDir,'wb') as outFile:
            pickle.dump(self.data,outFile)
    #Loads the Data
    def _loadData(self):
        with open(self.cacheDir,'rb') as inFile:
            self.data = pickle.load(inFile)
    """Functions for Server Interaction"""
    #Begins the Functions for Processing incoming Data
    def processData(self,infoDict):
        self._addTick(infoDict)
        #MakeDecision
        self._saveData()
    def _addTick(self,infoDict):
        infoDict = infoDict['quote']
        """Old Data Format
        self.conversionDict = {"ticker":"ticker",
                                "tickVolume":"Volume",
                                "tickOpenPrice":"Open",
                                "tickClosePrice":"Close",
                                "tickHighPrice":"High",
                                "tickLowPrice":"Low",
                                "tickStartTime":"Date"
                                }
        #Converting the infoDict to the pandasDict that we'll be adding
        renamedDict = {self.conversionDict[i]:infoDict[i] for i in self.conversionDict.keys()}
        """
        #Building a Large Dataframe with all the Data
        df = pd.DataFrame([infoDict['backtester']])
        #Converting out Date to an actual date
        df['Date'] = pd.to_datetime(df['Date'], unit='ms', origin='unix')
        #Setting it to our Index.
        df = df.set_index('Date')
        #Finding out if we've got this stock already or need to add it
        matchingStock = [i for i in self.data['stocks'] if i.ticker == infoDict['ticker']]
        if matchingStock:
            matchingStock[0].addData(df)
        else:
            #Making sure we keep a reference to our Data Organizer in the Stock
            s = stock(infoDict['ticker'],df)
            self.data['stocks'].append(s)
            #Matching the Format we'd have if we had the stock previously
            matchingStock = [s]
        #matchingStock[0]._makeDecision('FunctionWouldGoHere')
    



class stock:
    #Serverside Information
    serverIP = 'http://192.168.1.100'
    serverPort = 4444
    #Datetime Object used to let us know how far back of data we need to hold.
    datahold = datetime.timedelta(days = 3)
    def __init__(self,ticker,initialDF):
        self.ticker = ticker
        self.df = None
        #Holds how much of the stock we've bought
        self.purchased = False
        #Holds the Amount we're buying
        self.amount = None
        print(self.amount)
        #Builds in the Data Frame
        self.addData(initialDF)
        
    #Adds Data and see's if we need to get additional data.
    def addData(self,df):
        added = False
        #If we've got a Dataframe, append, Otherwise lets start a new one
        if type(self.df) == pd.DataFrame:
            #Making sure we don't have the Data already in the Dataframe.
            if not df.index[0] in self.df.index:
                self.df = self.df.append(df)
                #Marking that we've added Data.
                added = True
        #Starting a New DF and Indicating that we need to make decisions.
        else:
            self.df = df
            added = True
        #If we've modified our DF, Let's continue on
        if added:
            #Seeing if we need to get more data.
            if self.df.index.min() > self.df.index.max()-self.datahold or self.df.index.min() == self.df.index.max():
                self._getPolygonData()
            if self.df.index.max() - self.datahold - self.datahold > self.df.index.min():
                self._trimDF()
            #Sorting our Dataframe
            self.df = self.df.sort_index()
            import liveAlgorithms
            strat = liveAlgorithms.Scalper
            self._makeDecision(strat)
    #Filters the Dataframe to Trim old Data
    def _trimDF(self):
        self.df = self.df.loc[self.df.index >= self.df.index.max() - self.datahold]
    #Loads Historical Data from a Web Interface
    def _getPolygonData(self, timespan='minute'):
        #Doing some Error Checking
        if timespan not in ['minute','second','hour','day']:
            raise Exception('Timespan is not a valid entry')
        #Checking our dates
        import datetime
        startDatetime = self.df.index.max() - self.datahold - self.datahold
        endDatetime = self.df.index.min() - datetime.timedelta(minutes=1)
        #If Timespan is greater than a week, break down into multiple requests
        if (endDatetime - startDatetime).days>7:
            payloads = []
            currDate = startDatetime + datetime.timedelta(days=7)
            while currDate < endDatetime:
                #Constructing our Payload Dicts to send along with the URL
                payloads.append({'ticker':self.ticker,'startDate':startDatetime.strftime('%Y-%m-%d'),'endDate':currDate.strftime('%Y-%m-%d'),'timespan':timespan})
                startDatetime = currDate + datetime.timedelta(days=1)
                currDate = startDatetime + datetime.timedelta(days=7)
            payloads.append({'ticker':self.ticker,'startDate':startDatetime.strftime('%Y-%m-%d'),'endDate':currDate.strftime('%Y-%m-%d'),'timespan':timespan})
        #If we're less than a week we only need one request
        else:
            payloads = [{'ticker':self.ticker,'startDate':startDatetime.strftime('%Y-%m-%d'),'endDate':endDatetime.strftime('%Y-%m-%d'),'timespan':timespan}]
        #List of all Returned Data
        dictList = []
        for payload in payloads:
            import requests
            r = requests.post(self.serverIP+':' + str(self.serverPort) + '/' + 'getDateSpanHistory?',params = payload)
            dictList += r.json()
        #Building a Large Dataframe with all the Data
        df = pd.DataFrame([i['backtester'] for i in dictList])
        #Converting out Date to an actual date
        df['Date'] = pd.to_datetime(df['Date'], unit='ms', origin='unix')
        df = df.set_index('Date')
        #Filtering out any data that is equal to our greater than our date. This is important as the API is limited on Date not on Time
        df = df.loc[df.index < self.df.index.max()]
        #Appending the Data to our Dataset.
        self.df = self.df.append(df)
    #This makes the Decision Based on the Strategy
    def _makeDecision(self,strat):
        #Setting up our Strategy
        s = strat(self.df,self.purchased)
        s.buy = self._buy
        s.sell = self._sell
        #Calling the next Function
        s.next()

    #Determines what we do when we buy
    def _buy(self,percentage=0.05):
        import math
        #Doing something after here to tell Jezza to buy us a stock
        self.purchased = (self.df.index[-1]-datetime.datetime(1970,1,1)).total_seconds()
        #Determining what quantity to buy.
        cashToSpend = percentage * self._cash
        qty = math.floor(cashToSpend / self._currentCost)
        import copy
        self.amount = copy.copy(qty)
        #Getting Setup to Buy (Sending Ticker and Qty.)
        payload = {'ticker':self.ticker,'quantity':qty}
        #Sending the Web Request to the Server.
        import requests
        if SERVER_CONNECTION:
            r = requests.post(self.serverIP+':' + str(self.serverPort) + '/' + 'buy?',params = payload)
        else:
            print('Buying - ' + str(payload))
        
    #Executes what we do for a sell
    def _sell(self,percentage=1):
        if self.amount:
            #Getting Setup to Buy (Sending Ticker and Qty.)
            payload = {'ticker':self.ticker,'quantity':math.floor(self.amount*percentage)}
            #Clearing it out of our Purchases
            if self.amount and percentage == 1:
                self.amount = None
            elif self.amount and percentage != 1:
                self.amount = math.floor(self.amount*percentage)
            #Sending the Web Request to the Server.
            import requests
            if SERVER_CONNECTION:
                r = requests.post(self.serverIP+':' + str(self.serverPort) + '/' + 'sell?',params = payload)
            else:
                print('Selling - ' + str(payload))
    #Queries how much Cash we currently have
    @property
    def _cash(self):
        import requests
        r = requests.get(self.serverIP+':' + str(self.serverPort) + '/' + 'cash')
        return r.json()
    #Returns the Current Cost of the Asset
    #Uses the High of the Last period to take a Conservative Cost
    @property
    def _currentCost(self):
        return self.df['High'][-1]
    #Checks to see if we should be trailingStopSelling (Depricated)
    def _trailingLoss(self,lossMargin=0.05):
        #Calculating the Max Price
        df = self.df.loc[self.df.index >= self.purchased]
        maxPrice = df['Close'].max()
        #Seeing if we should sell.
        if self.df['Close'][-1] <= maxPrice * (1-lossMargin):
            self._sell()
        
if __name__ == "__main__":
    #import pandas as pd
    #a = stock('Test',pd.DataFrame())
    #a._cash()
    pass