"""
Contains the Classes needed for organizing our Test Datasets
testingDataOrganizer - Organizes all of the different test sets, adds new ones, provides information about what we have
testingData - Organizes the data from a singular test set, provides the dataframe required for Backtesting

"""

import os
import pandas as pd
import json
import datetime



"""Holds all the Information and Functions regarding to our Testing Datasets.
Functions include getting additional testing data from our Express endpoint for Polygon.io
also includes information summerizing the datasets themselves"""
class testingDataOrganizer():
    def __init__(self,cacheDir = 'testingData'):
        #Making sure we've got a Testing Data dir.
        if not 'testingData' in os.listdir('.'):
            os.mkdir('testingData')
        #Seeing if we've got our dataIndex.json in the testingData dir
        if not 'dataIndex.json' in os.listdir('testingData'):
            #Instantiating our List of Data Sources
            self.sources = []
        else:
            self._readDictionary()
    #Writes the JSON Dictionary
    def _writeDictionary(self):
        jsonDicts = [i.returnData() for i in self.sources]
        with open(os.path.join('testingData','dataIndex.json'),'w') as outFile:
            outFile.write(json.dumps(jsonDicts))
    #Reads the Dictionary
    def _readDictionary(self):
        #Reading the JSON Index In
        with open(os.path.join('testingData','dataIndex.json'),'r') as inFile:
            sources = json.loads(inFile.read())
        #Instantiating all of the Sourecs
        self.sources = [testingData(*[i[x] for x in i.keys()]) for i in sources]
    #Adds a new Stock to the Data Organizer
    def addStock(self,ticker='DIA',startDate = '2020-01-01',endDate = '2020-04-15', timespan='minute', name = None, fileLoc = None, group = None):
        #Getting the Stock and appending it to our Sources
        self.sources.append(testingData(ticker = ticker,startDate = startDate,endDate = endDate,timespan=timespan, name = name, fileLoc = fileLoc,group = group))
        #Writing out our Data Dictionary
        self._writeDictionary()
    #Returns the Dataframes within the Datasets.
    def returnDataframes(self):
        return [i.dataFrame for i in self.sources]
    #Returns Specific BT Datafiles based on Name
    def returnData(self,names):
        if type(names) == str:
            return [i for i in self.sources if i.name == names][0]
        elif type(names) == list:
            return [i for i in self.sources if i.name in names]
    #Returns Resampled Data for Specific 
    def returnSampledData(self,names,timeFrame):
        #See Valid Time Aliases Here
        #https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#offset-aliases
        #Making a List of a Single Name
        if type(names) == str:
            names = [names]
        #Returning the Resampled Datasets in Pandas Dataframes
        dfs = [i.dataFrame.resample(timeFrame).mean().dropna() for i in self.sources if i.name in names]
        #Transforming Dataframes into Backtesting compatible Data Sources
        import backtrader as bt
        bts = [bt.feeds.PandasData(dataname=i) for i in dfs]
        #Returning the Finalized Data
        if len(bts) == 1:
            bts = bts[0]
        return bts

"""Holds information related to an individual testing dataset
Has the functions required to either load that dataset from a cached file,
or download the dataset from our Express endpoint from Polygon.io"""
class testingData():
    def __init__(self,ticker,startDate,endDate,timespan,name,fileLoc,group):
        #Base Data about the Stock
        self.ticker = ticker
        self.startDate = startDate
        self.endDate = endDate
        self.timespan = timespan
        self.name = name
        self.group = group
        #If we've got a File Location load the Data from this.
        if fileLoc:
            #We've got a fileLoc, load the DATAAAAAAAAAAAA
            self.fileLoc = fileLoc
            self._loadPickle()
        #Otherwise we should be making 
        else:
            self._getPolygonData(ticker = self.ticker,
                                startDate = self.startDate,
                                endDate = self.endDate,
                                timespan = self.timespan)
            self.fileLoc = os.path.join('testingData',self._findFilename())
            self._savePickle()
    #Returns the Backtrader Data Object
    @property
    def btData(self):
        import backtrader as bt
        return bt.feeds.PandasData(dataname=self.dataFrame)
    #Shortcut to the Below returnData
    @property
    def info(self):
        return self.returnData()
    #Returns the Data
    def returnData(self):
        return {'ticker':self.ticker,'startDate':self.startDate,'endDate':self.endDate,'timespan':self.timespan,'name':self.name,'fileLoc':self.fileLoc,'group':self.group}
    #Loading JSON Data
    def _loadPickle(self):
        self.dataFrame = pd.read_pickle(self.fileLoc)
    #Saving our JSON Data
    def _savePickle(self):
        self.dataFrame.to_pickle(self.fileLoc)
        #with open(self.fileLoc,'w') as outFile:
        #    outFile.write(self.dataFrame.to_json(orient='index'))
    #Finds the next filename to use
    def _findFilename(self):
        if self.name + '.data' in os.listdir('testingData'):
            raise Exception ('Filename ' + self.name + ' is already in use')
        return self.name + '.data'
    #Loads Historical Data from a Web Interface
    def _getPolygonData(self,ipAddress = 'http://192.168.1.100',port = 4444, ticker='DIA',startDate = '2018-01-01',endDate = '2020-04-15', timespan='minute'):
        #Doing some Error Checking
        if timespan not in ['minute','second','hour','day']:
            raise Exception('Timespan is not a valid entry')
        #Checking our dates
        import datetime
        startDatetime = datetime.datetime.strptime(startDate,'%Y-%m-%d').date()
        endDatetime = datetime.datetime.strptime(endDate,'%Y-%m-%d').date()
        #If Timespan is greater than a week, break down into multiple requests
        if (endDatetime - startDatetime).days>7:
            payloads = []
            currDate = startDatetime + datetime.timedelta(days=7)
            while currDate < endDatetime:
                #Constructing our Payload Dicts to send along with the URL
                payloads.append({'ticker':ticker,'startDate':startDatetime.strftime('%Y-%m-%d'),'endDate':currDate.strftime('%Y-%m-%d'),'timespan':timespan})
                startDatetime = currDate + datetime.timedelta(days=1)
                currDate = startDatetime + datetime.timedelta(days=7)
            payloads.append({'ticker':ticker,'startDate':startDatetime.strftime('%Y-%m-%d'),'endDate':currDate.strftime('%Y-%m-%d'),'timespan':timespan})
        #If we're less than a week we only need one request
        else:
            payloads = [{'ticker':ticker,'startDate':startDatetime.strftime('%Y-%m-%d'),'endDate':endDatetime.strftime('%Y-%m-%d'),'timespan':timespan}]
        #List of all Returned Data
        dictList = []
        for payload in payloads:
            import requests
            r = requests.post(ipAddress+':' + str(port) + '/' + 'getDateSpanHistory?',params = payload)
            dictList += r.json()
        #Building a Large Dataframe with all the Data
        df = pd.DataFrame(dictList)
        #Converting out Date to an actual date
        df['Date'] = pd.to_datetime(df['t'], unit='ms', origin='unix')
        #Renaming our Datafields
        df['Open'] = df['o']
        df['Close'] = df['c']
        df['Low'] = df['l']
        df['High'] = df['h']
        df['Volume'] = df['v']
        df = df.set_index('Date')
        self.dataFrame = df[['Open','High','Low','Close','Volume']].copy()
    """Functions for Returning Mimicked Data in a Web Format"""
    def getWebdata(self):
        for date,row in self.dataFrame.T.iteritems():
            #New Format
            yield {'backtester':{'Date':timesinceepoch(date),
                                    'Open':row['Open'],
                                    'Close':row['Close'],
                                    'High':row['High'],
                                    'Low':row['Low'],
                                    'Volume':row['Volume']},
                    'ticker':self.ticker,
                    'eventType':'AM'}
            ''' old Data Format
            yield {'eventType':'AM',
            'ticker':self.ticker,
            'tickVolume':row['Volume'],
            'accumulatedDayVolume':1,
            'dayOpeningPrice':1,
            'volumeWeightedAveragePrice':1,
            'tickOpenPrice':row['Open'],
            'tickClosePrice':row['Close'],
            'tickHighPrice':row['High'],
            'tickLowPrice':row['Low'],
            'tickAveragePrice':1,
            'tickStartTime':timesinceepoch(date),
            'tickEndTime':timesinceepoch(date)}
            '''

def timesinceepoch(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds() * 1000.0

def proxyWebserver(source,url = 'http://127.0.0.1:5000/price', rate = 0.25, assets = []):
    import requests,json,time
    #Setting up our Headers for the Web Request
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    n = 0
    for dataPoint in source.getWebdata():
        dataDict = {'quote':dataPoint,'positions':assets}
        r = requests.post(url, data=json.dumps(dataDict), headers=headers)
        print('Postin data')
        #Taking a Delay Time
        time.sleep(rate)

    return

if __name__ == '__main__':
    t = testingDataOrganizer()
    #proxyWebserver(t.sources[0])
    #print([i for i in t.sources[0].getWebdata()][0])
    #print([i.info for i in t.sources])
    data = t.returnData('testSet_12')
    #print(data.dataFrame)
    #t.addStock(name='testSet_3')
    #t.addStock(name = 'testSet_17',ticker='SPX', startDate='1992-01-01', endDate='1993-01-01', group = 'Index')
    #t.addStock(name = 'testSet_4',ticker='SPCE', startDate='2019-12-13', endDate='2020-04-01', group = 'Space')
    #t.addStock(name = 'testSet_5',ticker='AJRD', startDate='2019-12-13', endDate='2020-04-01', group = 'Space')
    #t.addStock(name = 'testSet_6',ticker='LMT', startDate='2019-12-13', endDate='2020-04-01', group = 'Space')
    #t.addStock(name = 'testSet_7',ticker='TXT', startDate='2019-12-13', endDate='2020-04-01', group = 'Space')
    #t.addStock(name = 'testSet_8',ticker='DIS', startDate='2019-12-13', endDate='2020-04-01', group = 'Entertainment')
    #t.addStock(name = 'testSet_9',ticker='MANU', startDate='2019-12-13', endDate='2020-04-01', group = 'Entertainment')
    #t.addStock(name = 'testSet_10',ticker='SEAS', startDate='2019-12-13', endDate='2020-04-01', group = 'Entertainment')
    #t.addStock(name = 'testSet_11',ticker='WWE', startDate='2019-12-13', endDate='2020-04-01', group = 'Entertainment')
    #t.addStock(name = 'testSet_12',ticker='DAL', startDate='2019-12-13', endDate='2020-04-01', group = 'Airlines')
    #t.addStock(name = 'testSet_13',ticker='AAL', startDate='2019-12-13', endDate='2020-04-01', group = 'Airlines')
    #t.addStock(name = 'testSet_14',ticker='UAL', startDate='2019-12-13', endDate='2020-04-01', group = 'Airlines')
    #t.addStock(name = 'testSet_15',ticker='JBLU', startDate='2019-12-13', endDate='2020-04-01', group = 'Airlines')
