from backtesting import Strategy
import talib as ta
from backtesting.lib import crossover,resample_apply
from backtesting.lib import SignalStrategy, TrailingStrategy
import pandas as pd
import ast

"""Class to Keep Information about the Algorithms that Reside within this File"""
class algorithmInformation():
    def __init__(self):
        #Opening the file with our Algorithms in it
        with open(__file__) as thisFile:
            readFile = thisFile.read()
        self._findStrategies(readFile)
        #self.getStrategyVariables('demoStrategy')
    #Iterating through our Classes in this File and Returning those inhereiting a Strategy 
    #Returns ast.ClassDef objects with the parsed Classes for our Strategies
    def _findStrategies(self,pythonCode):
        module = ast.parse(pythonCode)
        #Looking for Classes
        classObjs = [i for i in ast.walk(module) if isinstance(i, ast.ClassDef)]
        #Finding Classes that have a Superclass
        strategies = [i for i in classObjs if i.bases]
        #Filtering for Superclasses that have Strategy in them
        self.strategies = [i for i in strategies if any(['Strategy' in x.id for x in i.bases])]
    #Returns a list of class Variables for a Strategy with a Given Name
    def getStrategyVariables(self,stratName):
        #Filtering to our Strategy
        nameMatch = [i for i in self.strategies if i.name == stratName]
        if len(nameMatch) != 1:
            raise Exception('No Name Match in Algorithm File')
        nameMatch = nameMatch[0]
        #Finding our Assign objects which live in the Class Space
        return [i.targets[0].id for i in nameMatch.body if isinstance(i, ast.Assign)]
                

class demoStrategy(Strategy):
    """Baseline Strategy demo from Backtesting.py"""
    d_rsi = 30  # Daily RSI lookback periods
    w_rsi = 30  # Weekly
    level = 70
    
    def init(self):
        # Compute moving averages the strategy demands
        self.ma10 = self.I(ta.SMA, self.data.Close, 10)
        self.ma20 = self.I(ta.SMA, self.data.Close, 20)
        self.ma50 = self.I(ta.SMA, self.data.Close, 50)
        self.ma100 = self.I(ta.SMA, self.data.Close, 100)
        
        # Compute daily RSI(30)
        self.daily_rsi = self.I(ta.RSI, self.data.Close, self.d_rsi)
        
        # To construct weekly RSI, we can use `resample_apply()`
        # helper function from the library
        self.weekly_rsi = resample_apply(
            'W-FRI', ta.RSI, self.data.Close, self.w_rsi)
        
        
    def next(self):
        price = self.data.Close[-1]
        
        # If we don't already have a position, and
        # if all conditions are satisfied, enter long.
        if (not self.position and
            self.daily_rsi[-1] > self.level and
            self.weekly_rsi[-1] > self.level and
            self.weekly_rsi[-1] > self.daily_rsi[-1] and
            self.ma10[-1] > self.ma20[-1] > self.ma50[-1] > self.ma100[-1] and
            price > self.ma10[-1]):
            
            # Buy at market price on next open, but do
            # set 8% fixed stop loss.
            self.buy(sl=(1-0.01) * price)
        
        # If the price closes 2% or more below 10-day MA
        # close the position, if any.
        elif price < (1-0.05) * self.ma10[-1]:
            self.position.close()

#Our Simple Strategy
class demoStrategy2(SignalStrategy,
               TrailingStrategy):
    """Base SMA Crossover Technique for Testing"""
    # Define the two MA lags as *class variables*
    # for later optimization
    n1 = 12
    n2 = 29
    
    def init(self):
        # In init() and in next() it is important to call the
        # super method to properly initialize all the classes
        super().init()
        # Precompute two moving averages
        sma1 = self.I(ta.SMA, self.data.Close, self.n1)
        sma2 = self.I(ta.SMA, self.data.Close, self.n2)
    
        # Taking a first difference (`.diff()`) of a boolean
        # series results in +1, 0, and -1 values. In our signal,
        # as expected by SignalStrategy, +1 means buy,
        # -1 means sell, and 0 means to hold whatever current
        # position and wait. See the docs.
        signal = (pd.Series(sma1) > sma2).astype(int).diff().fillna(0)
        # Set the signal vector using the method provided
        # by SignalStrategy
        self.set_signal(signal)
        
        # Set trailing stop-loss to 4x ATR
        # using the method provided by TrailingStrategy
        self.set_trailing_sl(1)


if __name__ == '__main__':
    t = algorithmInformation()
    