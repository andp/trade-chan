
class taLibFunction():
    def __init__(self,name,category):
        
        self.name = name
        self.category = category
        self.dict = {'name':name,
                    'category':category}
        #Getting the Description
        self._getDescription()
    #Breaks down the Function Description
    def _getDescription(self):
        import talib
        funct = getattr(talib,self.name)
        description = funct.__doc__
        splitLines = description.split('\n')
        breaks = [index for index,element in enumerate(splitLines) if element == '']
        self.functionName = splitLines[:breaks[0]][0].strip()
        self.description = splitLines[breaks[0]+1:breaks[1]][0].strip()
        self.dict['description'] = self.description
        self.dict['source'] = 'TA-Lib'
        #Finding Tab Seperations for Input/Output/Parameter Sections
        inputOutputSection = splitLines[breaks[1]+1:]
        sections = [(index,element) for index,element in enumerate(inputOutputSection) if (len(element)-len(element.lstrip())) == 4]
        for start,end in zip(sections[:-1],sections[1:]):
            sectionName = start[1]
            items = inputOutputSection[start[0]+1:end[0]]
            self.dict[sectionName.strip().replace(':','').lower()] = [i.strip() for i in items]
    #Returns the JSON Representation of the Object
    @property
    def json(self):
        import json
        return json.dumps(self.dict)
            

class defaultComparitor():
    def __init__(self,name,category,description,source = ''):
        self.dict = {'name':name}
        print('hi')

def buildComparitors():
    compList = []
    #Making our Internal Comps
    compList.append({'name':'Greater',
                    'category':'Comparitors',
                    'description':'Greater than Function',
                    'source':'python',
                    'inputs':['real','real'],
                    'outputs':['boolean']})
    compList.append({'name':'Less',
                    'category':'Comparitors',
                    'description':'Less than Function',
                    'source':'python',
                    'inputs':['real','real'],
                    'outputs':['boolean']})
    compList.append({'name':'Greater_or_Equal',
                    'category':'Comparitors',
                    'description':'Greater than or Equal To Function',
                    'source':'python',
                    'inputs':['real','real'],
                    'outputs':['boolean']})
    compList.append({'name':'Less_or_Equal',
                    'category':'Comparitors',
                    'description':'Less than or Equal To Function',
                    'source':'python',
                    'inputs':['real','real'],
                    'outputs':['boolean']})
    compList.append({'name':'Equivalent',
                    'category':'Comparitors',
                    'description':'Equal To Function',
                    'source':'python',
                    'inputs':['real','real'],
                    'outputs':['boolean']})
    #Logic Functions
    compList.append({'name':'And',
                    'category':'Comparitors',
                    'description':'And To Function',
                    'source':'python',
                    'inputs':['boolean','boolean'],
                    'outputs':['boolean']})
    compList.append({'name':'Or',
                    'category':'Comparitors',
                    'description':'Or Function',
                    'source':'python',
                    'inputs':['boolean','boolean'],
                    'outputs':['boolean']})
    compList.append({'name':'Xor',
                    'category':'Comparitors',
                    'description':'Exclusive Or Function',
                    'source':'python',
                    'inputs':['boolean','boolean'],
                    'outputs':['boolean']})
    compList.append({'name':'Not',
                    'category':'Comparitors',
                    'description':'Not Function',
                    'source':'python',
                    'inputs':['boolean'],
                    'outputs':['boolean']})
    #Math Functions
    compList.append({'name':'Sum',
                    'category':'Reduce Functions',
                    'description':'List Summation',
                    'source':'python',
                    'inputs':['real: (any ndarray)'],
                    'outputs':['real']})
    compList.append({'name':'Median',
                    'category':'Reduce Functions',
                    'description':'Median of a List',
                    'source':'python',
                    'inputs':['real: (any ndarray)'],
                    'outputs':['real']})
    compList.append({'name':'Mode',
                    'category':'Reduce Functions',
                    'description':'Mode of a List Summation',
                    'source':'python',
                    'inputs':['real: (any ndarray)'],
                    'outputs':['real']})
    return compList


#Populates taLibFunctions
def findFunctions():
    import talib
    groupListing = talib.get_function_groups()
    functions = []
    for group in groupListing.keys():
        for function in groupListing[group]:
            functions.append(taLibFunction(function,group))
    return functions


if __name__ == "__main__":
    #Returns all the Constructors we have in TA Lib to Build With
    a = findFunctions()
    #Returns the JSON Representation of all the Constructors.
    import os,pickle
    with open(os.path.join('demoFiles','sampleFunctions.p'),'wb') as demoFile:
        pickledOutput = pickle.dumps([i.dict for i in a]+buildComparitors())
        demoFile.write(pickledOutput)
    pass