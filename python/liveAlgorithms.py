import talib
import numpy as np
import datetime

class liveTrader():
    def __init__(self,stock):
        #Holds the Data (This is added from Previous Function)
        self.data = stock.df
        #Determines if we've purchased it or not
        self.purchased = stock.purchased
        self.amount = stock.amount
        self.outstanding = stock.outstanding
    #Exceutes our Buy Internally. Then Executes the Buy() function which sends the info to the servah
    def _buy(self):
        #Sets our Buy Period if we don't own it already.
        if not self.purchased and not self.outstanding:
            print(self.purchased)
            #Executes the Buy Function set by the Server
            self.buy(percentage = 0.05)
    #Executes our Sell Internally, then Executes Sell() which sends the info to the servah
    def _sell(self):
        #Sets our Buy Period if we don't own it already.
        if self.purchased and not self.outstanding:
            self.purchased = False
            #Executes the Sell Function set by the Server
            self.sell(percentage = 1)
    #Does our Stopless
    def stopLoss(self,trailingLoss=0.05):
        #Calculating the Max Price
        df = self.data.loc[self.data.index >= self.purchased]
        maxPrice = df['Close'].max()
        #Seeing if we should sell.
        if self.data['Close'][-1] <= maxPrice * (1-trailingLoss):
            self._sell()
    #Parses out the Parameters from the Backtrader Setup
    def _parseParams(self,param):
        return [i[1] for i in self.params if i[0] == param][0]
    #Takes a Datetime and returns Secs since Epoch
    def _toSeconds(self,datetimeObj):
        return (datetimeObj-datetime.datetime(1970,1,1)).total_seconds()
    #Looks to make sure we have data for all the indicators we've got for the lookback period supplied.
    #Input - indicators = [talib.function,...] (talib Functions), maxLookback = -2 (Negative Int)
    #Output - Boolean (If True we've got Indicators for the Entire period.)
    def _fullIndicators(self,indicators,maxLookback):
        #Using List Comprehension to see if we've got any NaN's in our list of Indicator Outputs
        booleans = [[np.isnan(dataPoint) for dataPoint in ind[len(ind)+maxLookback:]] for ind in indicators]
        #Seeing if we've got any Nan's, then returning the inverse to indicate that if there weren't any Nans, we're good to continue
        return not any(booleans)


# Create a Stratey
class Scalper(liveTrader):
    params = (('periodSmall', 20),
                ('trailPercent',0.05),
                ('buyMulti',1))
    def __init__(self,data):
        liveTrader.__init__(self,data)
        #Determines the Maximum Period of Data we need
        #In this case, it's 15 , plus a -2 index (which makes it 16)
        self.dataPeriod = 16
        #Defining our Sizer
        self.order = None
        #Our Metrics we'll be trading on
        self.smaLow = talib.SMA(self.data.Close, timeperiod=self._parseParams("periodSmall"))
    def next(self):
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
        #Buying if we're crossing and not currently holding
        if self._fullIndicators and not self.purchased and len(self.data) > self.dataPeriod:
            if (self.data.Close[-2] < self.smaLow[-2]) and (self.data.Close[-1] > self.smaLow[-1]):
                self._buy()
        #If we have a Position let's consider selling
        elif self.purchased:
            self.stopLoss(trailingLoss=self._parseParams("trailPercent"))

# Create a Stratey
class Stupid(liveTrader):
    params = (('periodSmall', 20),
                ('trailPercent',0.01),
                ('buyMulti',1))
    def __init__(self,data):
        liveTrader.__init__(self,data)
        #Determines the Maximum Period of Data we need
        #In this case, it's 15 , plus a -2 index (which makes it 16)
        self.dataPeriod = 16
        #Defining our Sizer
        self.order = None
        #Our Metrics we'll be trading on
    def next(self):
        print('  "' + str(bool(self.purchased)) + ' - ' + str(len(self.data)) + ' - ' + str(self.data.Close[-1]) + '"  ')
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
        #Buying if we're crossing and not currently holding
        if not bool(self.purchased):
            if str(self.data.Close[-1])[-1] == '6':
                print('Buying - ' + str(self.data))
                self._buy()
        #If we have a Position let's consider selling
        elif bool(self.purchased):
            self.stopLoss(trailingLoss=self._parseParams("trailPercent"))

if __name__ == "__main__":
    Scalper()
    pass