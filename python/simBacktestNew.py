"""
Backtesting using Backtrader
Hopefully accomplishing the same stuff as previous simBacktest.py
"""

import backtrader as bt
import math
import time

"""
Class that Organizes the Backtesting of an Algorithm on a Specific Dataset
"""
class backtester():
    def __init__(self,dataSet,analyzers,initialCash = 20000, commission = 0):
        #Saving our Data
        self.data = dataSet
        #Saving our Analyzers We'll use
        self.analyzers = analyzers
        #Setting the Initial Cash
        self.cash = initialCash
        #Comission per Trade
        self.commission = commission
    #Function to Run a Backtest
    def runBacktest(self,algorithm,**parameters):
        cerebro = bt.Cerebro()
        #Initial Setup
        cerebro.broker.setcash(self.cash)
        cerebro.broker.setcommission(self.commission)
        #Adding Data and Strategy
        cerebro.adddata(self.data.btData)
        #Dealing with the case of having Multiple Requests at Once
        if parameters and type(parameters[list(parameters.keys())[0]]) == list:
            print('Woohoo')
            cerebro.optstrategy(algorithm,**parameters)
        else:
            cerebro.addstrategy(algorithm,**parameters)
        #Adding our Analyzers
        for anal in self.analyzers:
            #Setting a Name 
            cerebro.addanalyzer(anal, _name=self._formatAnalyzerName(anal))
        #Telling how long it takes.
        processStart = time.process_time()
        stratrun = cerebro.run()
        processTime = time.process_time() - processStart
        cerebro.plot()
        print(cerebro.broker.getvalue())
        return self._processResults(stratrun,processTime)
    #Process Results
    def _processResults(self,stratruns,processTime):
        results = []
        #Nesting the Results from a Single Run to Process the same as a Multirun
        if len(stratruns) == 1:
            stratruns = [stratruns]
        #Iterating through the results
        for stratrun in stratruns:
            for run in stratrun:
                resDict = {'processTime':processTime}
                #Writing out the Information about the Dataset
                resDict['dataset'] = self.data.info
                #Writing out the Parameters
                resDict['parameters'] = {key:run.p._getkwargs()[key] for key in run.p._getkwargs()}
                resDict['analysis'] = {}
                #Adding the Analysis
                for anal in run.analyzers:
                    analName = str(anal).split('.')[-1].split(' ')[0].lower()
                    #Adding all the Output from the Analysis to the Analysis Dictionary to Return
                    resDict['analysis'].update({key:anal.get_analysis()[key] for key in anal.get_analysis()})
                results.append(resDict)
        return results
    #Formats the name of an Analyzer
    def _formatAnalyzerName(self,analyzer):
        return str(analyzer).split('\'')[1].split('.')[-1]

# Create a Stratey
class CloseSMA(bt.Strategy):
    params = (('periodSmall', 150),
                ('periodLarge',300),
                ('trailPercent',.05),
                ('buyMulti',10),
                ('bPeriod',300))

    def __init__(self):
        #Defining our Sizer
        self.sizer = LongOnly()
        self.sizer.p.buyMulti = self.p.buyMulti
        #Our Metrics we'll be trading on
        self.smaLow = bt.indicators.SMA(self.data.close, period=self.p.periodSmall)
        self.smaHigh = bt.indicators.SMA(self.data.close, period=self.p.periodLarge)
        self.bbands = bt.talib.BBANDS(self.data.close, period=self.p.bPeriod)
        self.crossover = bt.indicators.CrossOver(self.smaLow,self.smaHigh)

    def next(self):
        if self.crossover > 0:
            #self.buy(exectype = bt.Order.StopTrail,trailpercent = self.p.trailPercent)
            self.buy()
        elif self.crossover < 0:
            self.sell()

# Create a Stratey
class Scalper(bt.Strategy):
    params = (('periodSmall', 20),
                ('trailPercent',0.05),
                ('buyMulti',1))

    def __init__(self):
        #Defining our Sizer
        self.order = None
        #self.sizer = LongOnly()
        #self.sizer.p.buyMulti = self.p.buyMulti
        #Our Metrics we'll be trading on
        self.smaLow = bt.indicators.SMA(self.data.close, period=self.p.periodSmall)
        #self.crossover = bt.indicators.CrossOver(self.smaLow,self.data.close)
    def log(self, txt, dt=None):
        ''' Logging function fot this strategy'''
        dt = dt or self.datas[0].datetime.date(0)
        print('%s, %s' % (dt.isoformat(), txt))

    def notify_order(self, order):
        if order.status in [order.Submitted, order.Accepted]:
            # Buy/Sell order submitted/accepted to/by broker - Nothing to do
            return

        # Check if an order has been completed
        # Attention: broker could reject order if not enough cash
        if order.status in [order.Completed]:
            if order.isbuy():
                self.log(
                    'BUY EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                    (order.executed.price,
                     order.executed.value,
                     order.executed.comm))

                self.buyprice = order.executed.price
                self.buycomm = order.executed.comm
            else:  # Sell
                self.log('SELL EXECUTED, Price: %.2f, Cost: %.2f, Comm %.2f' %
                         (order.executed.price,
                          order.executed.value,
                          order.executed.comm))

            self.bar_executed = len(self)

        elif order.status in [order.Canceled, order.Margin, order.Rejected]:
            self.log('Order Canceled/Margin/Rejected')

        # Write down: no pending order
        self.order = None
    def next(self):
        # Check if an order is pending ... if yes, we cannot send a 2nd one
        if self.order:
            return
        #Buying if we're crossing and not currently holding
        if not self.position:
            if (self.data.close[-2] < self.smaLow[-2]) and (self.data.close[-1] > self.smaLow[-1]):
                #self.order = self.buy(exectype = bt.Order.StopTrail, trailpercent = self.p.trailPercent)
                self.tPrice = self.data.close[-1]
                self.order = self.buy()
        #If we have a Position let's consider selling
        elif self.position and self.tPrice * (1 - self.p.trailPercent) < self.data.close[-1]:
            self.order = self.sell()
        
        if self.position and self.data.close[-1] > self.tPrice:
            self.tPrice = self.data.close[-1]
        #elif (self.data.close[-2] > self.smaLow[-2]) and (self.data.close[-1] < self.smaLow[-1]):
        #    self.order = self.sell()


#Our Sizer
class LongOnly(bt.Sizer):
    params = (('stake', 1),
                ('buyMulti',10))

    def _getsizing(self, comminfo, cash, data, isbuy):
    #Buy Situation
        if isbuy:
            #Finding the Maximum Number of Shares we could buy.
            maxShares = math.floor(self.broker.getcash()/data[-1])
            #Getting the Ratio of our Indicators
            buyRatio = data[-1]/self.strategy.smaLow[-1] - 1
            #Applying the Multiplier
            buyRatio = buyRatio * self.p.buyMulti
            #If we're less than 0 don't buy
            if buyRatio < 0:
                buyRatio = 0
            #Determining how many Shares to buy
            if buyRatio > 1:
                buyRatio = 1
            shares = int(buyRatio * maxShares)
            return shares

    # Sell situation
        position = self.broker.getposition(data)
        if not position.size:
            return 0  # do not sell if nothing is open

        return position.size


if __name__ == '__main__':
    import simData
    #Loading in all our testData
    organizer = simData.testingDataOrganizer()    
    data = organizer.returnData('testSet_16')

    #Initializing the Backtester with 3 Analyzers, and our Dataset from Above
    a = backtester(data,[bt.analyzers.SharpeRatio,bt.analyzers.SQN,bt.analyzers.Returns])
    """Two Examples are Below, Running a Single Test Point in Parameters, and the Other running a Set"""
    print(a.runBacktest(Scalper,periodSmall = 20,trailPercent = 0.05, buyMulti=100))
    #print(a.runBacktest(CloseSMA,periodSmall = [30,45,60],periodLarge = [60,120,240]))
