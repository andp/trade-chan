# app.py
from flask import Flask, request, jsonify
from flask_cors import CORS
from os import environ, listdir
from os.path import isfile, join

app = Flask(__name__)
CORS(app)

@app.route('/health', methods=['GET'])
def health():
  return jsonify('ok')

# FE is posting to here
@app.route('/algorithm', methods=['GET', 'POST'])
def algorithm():
  # need to parse this out now
  return jsonify('uwu')

@app.route('/results', methods=['GET'])
def results():
  simLogPath = './simLogs'
  # TODO :: is there way to combine all this
  onlyfiles = [f for f in listdir(simLogPath) if isfile(join(simLogPath, f))]
  # strip *_Results
  filteredFiles = list(filter(lambda x: not "_Results" in x, onlyfiles))
  # strip .json ending
  strippedFiles = list(map(lambda y: y.replace('.json', ''), filteredFiles))
  return jsonify(strippedFiles)

@app.route('/results/<string:result>', methods=['GET'])
def specificResult(result):
  # idk wtf is happening here, this can probably be made better
  import json
  # read file
  with open(f'./simLogs/{result}.json', 'r') as myfile:
      data = json.loads(myfile.read())
  # parse file
  obj = data
  for key in obj.copy():
    obj[key.lower()] = obj.pop(key)
  with open(f'./simLogs/{result}_Results.json', 'r') as secondFile:
    secondData = json.loads(secondFile.read())
  obj['results'] = json.loads(secondData['results'])
  return jsonify(obj)

#Function to get Functions related to Algorithm Production
@app.route('/functions', methods=['GET','POST'])
def functions():
    import pickle,os
    #Loading in Pickled Sample Functions so that a Dependency on TALib isn't required for now
    with open(os.path.join('demoFiles','sampleFunctions.p'),'rb') as pickleFile:
        functionData = pickle.loads(pickleFile.read())
    return jsonify(functionData)

#Routes for getting info on our Data Sources
@app.route('/source/list', methods=['GET','POST'])
def sourceList():
    import simData
    dataOrganizer = simData.testingDataOrganizer()
    return jsonify([i.info for i in dataOrganizer.sources])

#Returning all of the Data from a Dataset
@app.route('/source/view', methods=['GET','POST'])
def sourceView():
    import simData
    dataOrganizer = simData.testingDataOrganizer()
    data = dataOrganizer.returnData(request.args.get('name'))
    return data.dataFrame.to_json(orient=request.args.get('orient'))

#Dealing with an Emission from the Websocket with a Market Price
@app.route('/price', methods=['GET','POST'])
def price():
    import liveData
    org = liveData.dataOrganizer()
    org.processData(request.json)
    return jsonify(1)

#debug = environ.get('APP_DEBUG') or True
host = environ.get('APP_HOST') or '127.0.0.1'

app.run(debug = True, host = host)



"""Test Queries that can be used to Check Functionality

functions
http://127.0.0.1:5000/functions

sourceList
http://127.0.0.1:5000/source/list

sourceView
http://127.0.0.1:5000/source/view?name=testSet_4&orient=records



"""
