import simData
import simAlgorithms
import simBacktest

#Loading in all our testData
organizer = simData.testingDataOrganizer()
data = organizer.sources

#Getting our Algorithm we're going to test
algo = simAlgorithms.demoStrategy2

#Loading them into the Backtester and Sending it to the Moon
backtester = simBacktest.test(algo,data)
print(backtester.test())
