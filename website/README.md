# Dev

run `yarn && yarn fullDev`

this will start both the python server as well as the develop instance of next

---

if you want to run the server and website separately then use `yarn dev`

go to [http://localhost:3000](http://localhost:3000)

---

# Prod

to prod deploy run `yarn start`
