module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
    'nextjs',
    'prettier',
    'prettier/react'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 11,
    sourceType: 'module'
  },
  plugins: ['react', 'react-hooks', 'prettier'],
  rules: {
    'prettier/prettier': [
      'error',
      {
        trailingComma: 'none',
        singleQuote: true
      }
    ],
    'no-console': [
      'error',
      {
        allow: ['info', 'warn', 'error']
      }
    ],
    quotes: ['error', 'single'],
    'comma-dangle': 0,
    'no-underscore-dangle': 'off',
    'react/prop-types': 'off',
    'max-len': 'off',
    'linebreak-style': 'off',
    'no-bitwise': [
      'error',
      {
        allow: ['~']
      }
    ],
    'no-plusplus': [
      'error',
      {
        allowForLoopAfterthoughts: true
      }
    ]
  }
};
