import React from 'react';
import Link from 'next/link';
import { ROUTES } from '../../constants';
import { header } from '../../sass/header.module.scss';

const Header = () => (
  <header className={header}>
    <nav>
      <ul>
        <li>
          <Link href="/">
            <a>trade-chan web gui</a>
          </Link>
        </li>
        <li>
          <Link href={ROUTES.ALGORITHM}>
            <a>algo</a>
          </Link>
        </li>
        <li>
          <Link href={ROUTES.RESULTS}>
            <a>results</a>
          </Link>
        </li>
        <li>
          <Link href={ROUTES.DATA}>
            <a>data</a>
          </Link>
        </li>
      </ul>
    </nav>
  </header>
);

export default Header;
