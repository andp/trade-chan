import React from 'react';
import Head from 'next/head';
import Header from './header';
// import { useFetchUser } from '../../lib/user';

function Layout({ children }) {
  // const {user, loading} = useFetchUser();
  // const isLoggedIn = !!user;
  return (
    <>
      <Head>
        <title>trade-chan web gui</title>
      </Head>

      <Header />
      <main>
        {/* <div className="container">{isLoggedIn ? children : <a href="/api/login">Login</a>}</div> */}
        {children}
      </main>

      <style jsx>
        {`
          main {
            padding: 5rem 0;
            flex: 1;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
          }
          .container {
            min-height: 75vh;
            padding: 0 0.5rem;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
          }
        `}
      </style>
    </>
  );
}

export default Layout;
