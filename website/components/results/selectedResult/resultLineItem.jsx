import React, { useState } from 'react';
import Tooltip from '../../general/tooltip';

/**
 * display line items
 * @param lineItem
 * @constructor
 */
const ResultLineItem = ({ lineItem }) => {
  const [collapsed, setCollapsed] = useState(true);
  const {
    '# Trades': numberOfTrades,
    'Equity Final [$]': equityFinalDollar,
    'Return [%]': returnPercent,
    'Win Rate [%]': winRatePercent,
    'Equity Peak [$]': equityPeakDollar,
    'Buy & Hold Return [%]': buyHoldReturnPercent,

    'Avg. Drawdown Duration': avgDrawdownDuration,
    'Max. Drawdown Duration': maxDrawdownDuration,
    'Max. Drawdown [%]': maxDrawdownPercent,
    'Avg. Drawdown [%]': avgDrawdownPercent,

    'Avg. Trade Duration': avgTradeDuration,
    'Max. Trade Duration': maxTradeDuration,

    SQN: sqn,
    'Sharpe Ratio': sharpeRatio,
    'Sortino Ratio': sortinoRatio,
    'Calmar Ratio': calmarRatio,
    'Expectancy [%]': expectancyPercent,
    'Exposure [%]': exposurePercent,
    n1,
    n2,

    Start: start,
    End: end,
    Duration: durationOfTrades,

    'Best Trade [%]': bestTradePercent,
    'Avg. Trade [%]': avgTradePercent,
    'Worst Trade [%]': worstTradePercent,

    Dataset: dataset
  } = lineItem;

  /**
   * @param {Number} duration
   * @return {
   *   milliseconds: {Number},
   *   seconds: {Number},
   *   minutes: {Number},
   *   hours: {Number},
   *   days: {Number}
   * }
   * @constructor
   */
  // TODO :: clean this up
  const calculateDuration = (duration) => ({
    milliseconds: duration,
    seconds: duration / 1000,
    minutes: duration / 1000 / 60,
    hours: duration / 1000 / 60 / 60,
    days: duration / 1000 / 60 / 60 / 24
  });
  // TODO :: abstract
  const selectTime = (duration) => {
    const times = Object.entries(calculateDuration(duration));
    for (let i = times.length - 1; i > 0; i--) {
      const [timeIncrement, timeValue] = times[i];
      if (timeValue >= 1) return `${timeValue.toFixed(2)} ${timeIncrement}`;
    }
    return '';
  };

  // TODO :: add isNaN() typechecking otherwise return input
  const twoDecimalPlaces = (number) => number.toFixed(2);

  return (
    <div className="card">
      {/* Heading */}
      <div>
        {/* TODO :: add anchor heading to to scroll to it */}
        <h3>
          {lineItem.Group} :: {lineItem.Ticker} ::{' '}
          {twoDecimalPlaces(returnPercent)}% Return{' '}
          <button type="button" onClick={() => setCollapsed(!collapsed)}>
            {collapsed ? '➕' : '➖'}
          </button>
        </h3>
        <hr />
      </div>
      {/* Body */}
      {!collapsed && (
        <div>
          <div>
            <p>overall info</p>
            <p>Number of Trades: {numberOfTrades}</p>
            <p>Final Equity: {equityFinalDollar}</p>
            <p>
              Return %: {twoDecimalPlaces(returnPercent)}{' '}
              <Tooltip tooltipText={returnPercent} />
            </p>
            <p>Win Rate %: {twoDecimalPlaces(winRatePercent)}</p>
            <p>Equity Peak $: {equityPeakDollar}</p>
            <p>
              Buy &amp; Hold Return %: {twoDecimalPlaces(buyHoldReturnPercent)}
            </p>
          </div>
          <div>
            <p>dates/times</p>
            <p>Start: {new Date(start).toString()}</p>
            <p>End: {new Date(end).toString()}</p>
            <p>Duration: {selectTime(durationOfTrades)}</p>
          </div>
          <div>
            <p>trade $/%</p>
            <p>Best Trade %: {twoDecimalPlaces(bestTradePercent)}</p>
            <p>Avg. Trade %: {twoDecimalPlaces(avgTradePercent)}</p>
            <p>Worst Trade %: {twoDecimalPlaces(worstTradePercent)}</p>
          </div>
          <div>
            <p>avg&amp;max</p>
            <p>Avg. Drawdown Duration: {selectTime(avgDrawdownDuration)}</p>
            <p>Max Drawdown Duration: {selectTime(maxDrawdownDuration)}</p>
            <p>Avg. Drawdown %: {twoDecimalPlaces(avgDrawdownPercent)}</p>
            <p>Max Drawdown %: {twoDecimalPlaces(maxDrawdownPercent)}</p>
            <p>Avg. Trade Duration: {selectTime(avgTradeDuration)}</p>
            <p>Max Trade Duration: {selectTime(maxTradeDuration)}</p>
          </div>
          <div>
            <p>other</p>
            <p>SQN: {sqn}</p>
            <p>Sharpe Ratio: {sharpeRatio}</p>
            <p>Sortino Ratio: {sortinoRatio}</p>
            <p>Calmar Ration: {calmarRatio}</p>
            <p>Expectancy %: {twoDecimalPlaces(expectancyPercent)}</p>
            <p>Exposure %: {twoDecimalPlaces(exposurePercent)}</p>
            {n1 && <p>n1: {n1}</p>}
            {n2 && <p>n2: {n2}</p>}
          </div>
          {/* TODO :: do something with retrieving data set on click */}
          <button type="button">get dataset {dataset}</button>
        </div>
      )}
    </div>
  );
};

export default ResultLineItem;
