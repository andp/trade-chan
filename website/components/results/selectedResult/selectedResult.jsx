import React from 'react';
import ResultsHeader from '../../../containers/results/resultsHeader';
import ResultLineItem from './resultLineItem';

const SelectedResult = ({ resultLineItems }) => {
  return (
    <div>
      {/* Heading */}
      {/* TODO :: probably need to setup redux to allow for expand/collapse from header? */}
      <ResultsHeader />
      {/* Body */}
      {resultLineItems.map((resultLineItem) => (
        <ResultLineItem
          key={`${resultLineItem.Group}-${resultLineItem.Ticker}`}
          lineItem={resultLineItem}
        />
      ))}
    </div>
  );
};

export default SelectedResult;
