import React from 'react';
import ResultsHeaderGroupsAndTickers from './resultsHeaderGroupsAndTickers';

/**
 * Display header for the selected result
 * @param {String} resultId
 * @param {String} algorithmDescription
 * @param {String} algorithmName
 * @param {Object} groupsAndTickers
 * @returns {*}
 * @constructor
 */
const ResultsHeader = ({
  resultId,
  algorithmDescription,
  algorithmName,
  groupsAndTickers
}) => {
  return (
    <>
      <h1>selected result: {resultId}</h1>
      <h2>algorithm description: {algorithmDescription}</h2>
      <h2>algorithm name: {algorithmName}</h2>
      <div>
        {Object.entries(groupsAndTickers).map(([key, values]) => (
          <ResultsHeaderGroupsAndTickers
            key={`results-header-${resultId}-${key}`}
            groupName={key}
            tickers={values}
          />
        ))}
      </div>
    </>
  );
};

export default ResultsHeader;
