import React, { useState } from 'react';

/**
 * Display group + children
 * @param {String} groupName
 * @param {Array<string>} tickers
 * @returns {*}
 * @constructor
 */
const ResultsHeaderGroupsAndTickers = ({ groupName, tickers }) => {
  // TODO :: figure out how to extract this + (+|-) button to component that can affect state here
  const [collapsed, setCollapsed] = useState(false);
  return (
    <>
      <h2>
        {groupName}{' '}
        <button type="button" onClick={() => setCollapsed(!collapsed)}>
          {collapsed ? '➕' : '➖'}
        </button>
      </h2>
      {!collapsed && (
        <ul>
          {tickers.map((ticker) => (
            <li key={ticker}>{ticker}</li>
          ))}
        </ul>
      )}
    </>
  );
};

export default ResultsHeaderGroupsAndTickers;
