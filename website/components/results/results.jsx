import React, { useEffect } from 'react';
import SelectedResult from '../../containers/results/selectedResult';

/**
 * Main results component
 * @param {Array<string>} results
 * @param {Function} loadResults
 * @param {Function} loadSpecificResult
 * @param {Boolean} isResultSelected
 * @param {String} selectedResultId
 * @returns {*}
 * @constructor
 */
const Results = ({
  results,
  loadResults,
  loadSpecificResult,
  isResultSelected,
  selectedResultId
}) => {
  // TODO :: more hook abstraction
  async function fetchServerFunctions() {
    const fetchedResults = await fetch('http://localhost:5000/results');
    const data = await fetchedResults.json();
    loadResults({ data });
  }

  async function fetchSpecificResult(resultId) {
    const specificResult = await fetch(
      `http://localhost:5000/results/${resultId}`
    );
    const data = await specificResult.json();
    loadSpecificResult({ data });
  }

  useEffect(() => {
    fetchServerFunctions();
  }, []);

  return (
    <div>
      <p>Select a result to load</p>
      {results.map((result) => (
        <button
          key={`button-${result}`}
          type="button"
          onClick={() => fetchSpecificResult(result)}
        >
          {result} {selectedResultId === result && '🔵'}
        </button>
      ))}
      {isResultSelected && <SelectedResult />}
    </div>
  );
};

export default Results;
