import React, { useState } from 'react';

const Tooltip = ({ tooltipText }) => {
  // TODO :: should we allow clicking on emoji to KEEP tooltip up?
  const [inHover, setHover] = useState(false);
  return (
    <span
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <span role="img" aria-label="information emoji">
        ℹ️
      </span>
      {inHover && <p>{tooltipText}</p>}
    </span>
  );
};

export default Tooltip;
