import React from 'react';

const InputSelect = ({ optionsArray, handleSelect }) => (
  <select onChange={handleSelect}>
    {optionsArray.map((option) => (
      <option key={option} value={option}>
        {option}
      </option>
    ))}
  </select>
);

export default InputSelect;
