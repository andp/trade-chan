import React, { useState } from 'react';
import { get, isObjectNotEmpty } from '../../utils';
import InputSelect from '../../containers/algo/inputSelect';
import { indicator, buttonContainer } from '../../sass/indicator.module.scss';

const Indicator = ({
  addInUseIndicator,
  indicators,
  type,
  indexNumber,
  deleteInUseIndicator
}) => {
  const [currentFunction, setCurrentFunction] = useState({});
  const selectIndicator = (event) => {
    const cursor = event.target.value.split(',');
    const indicatorData = get(indicators, cursor);

    setCurrentFunction(indicatorData);
    addInUseIndicator({
      indicatorData,
      type,
      indexNumber
    });
  };

  const {
    category,
    name,
    description,
    inputs,
    parameters,
    outputs
  } = currentFunction;
  const selectValue = category && name && String([category, name]);
  return (
    <div className={indicator}>
      {description && <p>{description}</p>}
      {/* TODO :: figure out why default value doesn't hold */}
      <select
        value={selectValue}
        defaultValue={selectValue}
        onChange={selectIndicator}
      >
        {indicators &&
          Object.entries(indicators).map(([key, value]) => (
            <optgroup label={key} key={key}>
              {Object.entries(value).map(([valueKey, valueValue]) => (
                <option value={[key, valueKey]} key={valueKey}>
                  {valueValue.description}
                </option>
              ))}
            </optgroup>
          ))}
      </select>
      <div className={buttonContainer}>
        <button
          type="button"
          onClick={() => deleteInUseIndicator({ type, indexNumber })}
        >
          delete{' '}
          <span role="img" aria-label="x emoji">
            ❌
          </span>
        </button>
      </div>
      {isObjectNotEmpty(currentFunction) && (
        <div>
          {inputs && (
            <>
              <p>inputs:</p>
              {inputs.map((input, index) => {
                const [inputName, inputType] = input.split(': ');
                return (
                  <div key={`${inputName}${indexNumber}-${index}`}>
                    <p>
                      {inputName} {inputType && `<${inputType}>`}
                    </p>
                    <p>
                      {index}:{' '}
                      <InputSelect
                        {...{ type, indexNumber }}
                        inputNumber={index}
                      />
                    </p>
                  </div>
                );
              })}
            </>
          )}
          {parameters && (
            <>
              <p>parameters: </p>
              {parameters.map((parameter) => {
                const [paramName, paramDefault] = parameter.split(': ');
                // TODO :: write handling for input change
                return (
                  <p key={paramName}>
                    {paramName}: <input placeholder={paramDefault} />
                  </p>
                );
              })}
            </>
          )}
          {outputs && (
            <>
              <p>outputs:</p>
              {outputs.map((output) => (
                <p key={output}>{output}</p>
              ))}
            </>
          )}
        </div>
      )}
      <hr />
    </div>
  );
};

export default Indicator;
