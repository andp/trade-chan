import React, { Fragment, useEffect } from 'react';
import Indicator from '../../containers/algo/indicator';
import { isObjectNotEmpty } from '../../utils';

const Algo = ({
  inUseIndicators,
  loadIndicators,
  addInUseIndicator,
  submitData
}) => {
  // TODO :: abstract to hooks/effects we use?
  // https://www.smashingmagazine.com/2020/07/custom-react-hook-fetch-cache-data/ - implement
  async function fetchServerFunctions() {
    const serverFunctions = await fetch('http://localhost:5000/functions');
    const data = await serverFunctions.json();
    loadIndicators({ data });
  }

  useEffect(() => {
    fetchServerFunctions();
  }, []);

  const handleClick = ({ type, indexNumber }) => {
    addInUseIndicator({
      indicatorData: {},
      type,
      indexNumber
    });
  };
  return (
    <>
      <h1>algo creation</h1>
      {/* Need to add ticker picker here I think */}
      {Object.entries(inUseIndicators).map(([type, indicatorGroup]) => (
        <Fragment key={type}>
          <h2>{type}</h2>
          {/* Can put some tootip here to explain what `type` does */}
          <button type="button" onClick={() => handleClick({ type })}>
            add&nbsp;
            {isObjectNotEmpty(indicatorGroup) ? 'another' : 'an'}
            &nbsp;indicator➕
          </button>
          {Object.entries(indicatorGroup).map(([key]) => (
            <Indicator key={`${type}${key}`} nameType={[type, key]} />
          ))}
          <hr />
        </Fragment>
      ))}
      <button type="button" onClick={() => submitData}>
        send to the server
      </button>
    </>
  );
};
export default Algo;
