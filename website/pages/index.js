import React from 'react';
import Link from 'next/link';
import Layout from '../components/layout/layout';
import { ROUTES } from '../constants';

const Home = () => (
  <Layout>
    <>
      <h1 className="title">trade-chan web gui</h1>
      <p className="description">choose somewhere to go</p>
      <a href="/api/logout">logout</a>
      <div className="grid">
        <Link href={ROUTES.RESULTS}>
          <a href="#" className="card">
            <h3>algo results? &rarr;</h3>
            <p>view results from previously run algos</p>
          </a>
        </Link>
        <Link href={ROUTES.ALGORITHM}>
          <a href="#" className="card">
            <h3>create algo for testing &rarr;</h3>
            <p>create an algo and send it off</p>
          </a>
        </Link>
        <Link href={ROUTES.DATA}>
          <a className="card">
            <h3>view data &rarr;</h3>
            <p>check existing data sets</p>
          </a>
        </Link>
      </div>
    </>
  </Layout>
);

export default Home;
