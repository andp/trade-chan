import React from 'react';
import Layout from '../components/layout/layout';
import Algo from '../containers/algo/algo';

const Algorithm = () => (
  <Layout>
    <Algo />
  </Layout>
);
export default Algorithm;
