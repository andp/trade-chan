import React from 'react';
import Layout from '../components/layout/layout';
import Results from '../containers/results/results';

const Data = () => (
  <Layout>
    <h1>Results</h1>
    <p>list of data</p>
    <Results />
  </Layout>
);
export default Data;
