export default {
  RESULTS: '/results',
  ALGORITHM: '/algorithm',
  DATA: '/data'
};
