import { connect } from 'react-redux';
import { loadResults, loadSpecificResult } from '../../store/slices/results';
import Results from '../../components/results/results';
import { isObjectNotEmpty } from '../../utils';

const mapState = (state) => {
  const { results } = state;
  const { inUseResult } = results;
  return {
    results: results.results,
    groupsAndTickers: inUseResult.groupsAndTickers,
    isResultSelected: isObjectNotEmpty(inUseResult),
    selectedResultId: inUseResult.id
  };
};

const mapDispatch = { loadResults, loadSpecificResult };

export default connect(mapState, mapDispatch)(Results);
