import { connect } from 'react-redux';
import ResultsHeader from '../../components/results/selectedResult/resultsHeader/resultsHeader';

const mapState = (state) => {
  const { results } = state;
  const { inUseResult } = results;
  return {
    algorithmName: inUseResult.algorithm.name,
    algorithmDescription: inUseResult.algorithm.description,
    groupsAndTickers: inUseResult.groupsAndTickers,
    resultId: inUseResult.id
  };
};

export default connect(mapState)(ResultsHeader);
