import { connect } from 'react-redux';
import SelectedResult from '../../components/results/selectedResult/selectedResult';

const mapState = (state) => ({
  resultLineItems: state.results.inUseResult.results
});

export default connect(mapState)(SelectedResult);
