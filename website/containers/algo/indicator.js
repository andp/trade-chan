import { connect } from 'react-redux';
import Indicator from '../../components/algo/indicator';
import {
  addInUseIndicator,
  loadIndicators,
  deleteInUseIndicator
} from '../../store/slices/indicator';
import { isBuySell, isObjectNotEmpty, filterByOutputType } from '../../utils';
import { INDICATORS } from '../../constants';

const mapState = (state, ownState) => {
  const [type, indexNumber] = ownState.nameType;
  const indicators = state.indicator.indicatorList;
  const filteredIndicators = {};
  if (isBuySell({ type })) {
    Object.assign(
      filteredIndicators,
      filterByOutputType({ indicators, outputType: INDICATORS.OUTPUTS.BOOLEAN })
    );
  }
  return {
    indicators: isObjectNotEmpty(filteredIndicators)
      ? filteredIndicators
      : indicators,
    type,
    indexNumber
  };
};

const mapDispatch = { loadIndicators, addInUseIndicator, deleteInUseIndicator };

export default connect(mapState, mapDispatch)(Indicator);
