import { connect } from 'react-redux';
import InputSelect from '../../components/algo/inputSelect';
import { isObjectNotEmpty } from '../../utils';
import { updateInUseIndicatorInput } from '../../store/slices/indicator';

const mapState = (state, ownProps) => {
  const optionsArray = [
    'tickVolume',
    'tickOpenPrice',
    'tickEndTime',
    'tickAveragePrice',
    'tickClosePrice',
    'accumulatedDayVolume',
    'tickStartTime',
    'tickLowPrice',
    'dayOpeningPrice',
    'tickHighPrice',
    'volumeWeightedAveragePrice'
  ];

  const { type, indexNumber, inputNumber } = ownProps;
  const indicatorList = { ...state.indicator.inUseIndicators[type] };
  const thisIndicator = indicatorList[indexNumber];
  // Ignore self
  if (indicatorList) {
    delete indicatorList[indexNumber];
  }
  // TODO :: is there a better place to put this filtering?
  if (isObjectNotEmpty(indicatorList)) {
    /**
     * Match types of outputs to inputs for the
     * given select function
     * @type {array}
     */
    const filteredIndicatorList = Object.values(indicatorList).filter(
      (indicator) =>
        isObjectNotEmpty(indicator) &&
        // TODO :: need to take into account functions with multiple outputs
        indicator.outputs[0] === thisIndicator.inputs[inputNumber]
    );
    if (filteredIndicatorList.length) {
      optionsArray.push(
        ...filteredIndicatorList.map((indicator) => {
          let indicatorText = indicator.description;
          // Compose more descriptive text
          if (isObjectNotEmpty(indicator.inputValues)) {
            const inputValuesValues = Object.values(indicator.inputValues);
            indicatorText += ` <with input${
              inputValuesValues.length > 1 ? 's' : ''
            }: ${inputValuesValues.join(' & ')}>`;
          }
          return indicatorText;
        })
      );
    }
  }
  return {
    optionsArray,
    type,
    indexNumber,
    inputNumber
  };
};

const mapDispatch = (dispatch, ownProps) => ({
  handleSelect: (e) =>
    dispatch(
      updateInUseIndicatorInput({
        indicatorData: e.target.value,
        type: ownProps.type,
        indexNumber: ownProps.indexNumber,
        inputNumber: ownProps.inputNumber
      })
    )
});

export default connect(mapState, mapDispatch)(InputSelect);
