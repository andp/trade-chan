import { connect } from 'react-redux';
import {
  addInUseIndicator,
  loadIndicators
} from '../../store/slices/indicator';
import Algo from '../../components/algo/algo';
import { post } from '../../actions/rest';

const mapState = (state) => ({
  inUseIndicators: state.indicator.inUseIndicators,
  submitData: post({
    data: state.indicator.inUseIndicators,
    url: 'http://localhost:5000/algorithm'
  })
});

const mapDispatch = { loadIndicators, addInUseIndicator };

export default connect(mapState, mapDispatch)(Algo);
