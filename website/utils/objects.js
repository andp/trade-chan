/**
 * get item at path from object
 * @param {object} object
 * @param {array<string>} cursor
 * @returns {boolean|object}
 */
export const get = (object, cursor) => {
  let loopedObject = object;
  for (let i = 0; i < cursor.length; i++) {
    if (loopedObject[cursor[i]]) loopedObject = loopedObject[cursor[i]];
    else return false;
  }
  return loopedObject;
};

/**
 * Determine if object is empty
 * @param {object} object
 * @returns {boolean}
 */
export const isObjectNotEmpty = (object) =>
  object && !!Object.getOwnPropertyNames(object).length;
