import { categorySort } from './arrays';
/**
 * returns true if `type` exists
 * and is either `buy` or `sell`
 * @param type
 * @returns {boolean}
 */
export const isBuySell = ({ type }) =>
  !!type && (type === 'buy' || type === 'sell');

/**
 * filter outputs by a certain type
 * @param indicators
 * @param outputType
 * @returns {{}}
 */
export const filterByOutputType = ({ indicators, outputType }) => {
  const filterList = {};
  Object.entries(indicators).forEach(([, indicatorGroup]) => {
    const filteredGroup = Object.values(indicatorGroup).filter(
      (indicator) => ~indicator.outputs.indexOf(outputType)
    );
    Object.assign(filterList, {
      // Only include if it has a value in it
      ...(!!filteredGroup.length && categorySort(filteredGroup))
    });
  });
  return filterList;
};
