/**
 * arrange array into categories
 * @param array
 * @returns {*}
 */
export const categorySort = (array) =>
  array.reduce(
    (acc, curr) =>
      Object.assign(acc, {
        [curr.category]: {
          ...acc[curr.category],
          [curr.name]: curr
        }
      }),
    {}
  );
