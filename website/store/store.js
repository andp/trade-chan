import { configureStore, combineReducers } from '@reduxjs/toolkit';
import indicator from './slices/indicator';
import results from './slices/results';

// TODO :: results should be prefetched from server and rendered in
const initializeStore = (preloadedState = {}) =>
  configureStore({
    reducer: combineReducers({ indicator, results }),
    preloadedState
  });
export const store = initializeStore();
