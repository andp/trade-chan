import { createSlice } from '@reduxjs/toolkit';
import { categorySort } from '../../utils';

const indicatorSlice = createSlice({
  name: 'indicator',
  initialState: {
    indicatorList: {},
    inUseIndicators: {
      strategy: {
        0: {}
      },
      graphing: {},
      buy: {},
      sell: {}
    }
  },
  reducers: {
    loadIndicators: {
      reducer(state, action) {
        const { data } = action.payload;
        Object.assign(state, { indicatorList: data });
      },
      prepare(payload) {
        const { data } = payload;
        const formattedData = categorySort(data);
        return { payload: { data: formattedData } };
      }
    },
    addInUseIndicator(state, action) {
      const { indicatorData, type } = action.payload;
      let { indexNumber } = action.payload;
      if (!indexNumber) {
        const inUseIndicatorKeys = Object.keys(state.inUseIndicators[type]);
        indexNumber =
          parseInt(inUseIndicatorKeys[inUseIndicatorKeys.length - 1], 10) + 1 ||
          0;
      }
      Object.assign(state.inUseIndicators[type], {
        ...state.inUseIndicators[type],
        [indexNumber]: indicatorData
      });
    },
    updateInUseIndicatorInput(state, action) {
      const { indicatorData, type, indexNumber, inputNumber } = action.payload;
      Object.assign(state.inUseIndicators[type][indexNumber], {
        inputValues: {
          ...state.inUseIndicators[type][indexNumber].inputValues,
          [inputNumber]: indicatorData
        }
      });
    },
    deleteInUseIndicator(state, action) {
      const { type, indexNumber } = action.payload;
      const filteredIndicators = Object.entries({
        ...state.inUseIndicators[type]
      }).filter(([entry]) => indexNumber !== entry);
      Object.assign(state.inUseIndicators, {
        [type]: Object.fromEntries(filteredIndicators)
      });
    }
  }
});

export const {
  loadIndicators,
  addInUseIndicator,
  deleteInUseIndicator,
  updateInUseIndicatorInput
} = indicatorSlice.actions;

export default indicatorSlice.reducer;
