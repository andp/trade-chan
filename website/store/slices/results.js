import { createSlice } from '@reduxjs/toolkit';

const resultsSlice = createSlice({
  name: 'results',
  initialState: {
    results: [],
    inUseResult: {}
  },
  reducers: {
    loadResults(state, action) {
      const { data } = action.payload;
      Object.assign(state.results, data);
    },
    loadSpecificResult: {
      reducer(state, action) {
        const { data } = action.payload;
        Object.assign(state, { inUseResult: data });
      },
      prepare(payload) {
        const { data } = payload;
        const groupsAndTickers = data.results.reduce((acc, curr) => {
          const { Group: currGroup, Ticker: currTicker } = curr;
          // TODO :: fix linter error
          // eslint-disable-next-line no-prototype-builtins
          if (!acc.hasOwnProperty(currGroup)) acc[currGroup] = [];
          if (!~acc[currGroup].indexOf(currTicker))
            acc[currGroup].push(currTicker);
          return acc;
        }, {});
        const formattedData = { ...data, groupsAndTickers };
        return { payload: { data: formattedData } };
      }
    }
  }
});

export const { loadResults, loadSpecificResult } = resultsSlice.actions;

export default resultsSlice.reducer;
